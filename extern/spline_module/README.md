# spline_module #

catkin package wrapping the original c++ library to construct and evaluate spline functions developed by [John Burkardt](http://people.sc.fsu.edu/~jburkardt/). 

The original library can be download from here: [SPLINE] (https://people.sc.fsu.edu/~jburkardt/cpp_src/spline/spline.html) 

We wrapp this library in a catkin package and we use it for splinning dynamic trajectories (i.e., trajectories compatible with certain system dynamics)

We preserve the original terms and conditions of the original library explained [here](gnu_lgpl.txt)

### What is this package for? ###

This class provides mechanism to create (cubic hermite) splines of a system trajectory. 

Instead of using pure path splines, given a trajectory and a system dynamics, with this class you can sample your trajectory at different frequencies taking into account the dynamics of the system.

This is very useful in trajectory optimization and for solving riccati differential equations around trajectories (i.e., numerically integrating a continuous function that depends on your trajectories).

* Version
0.0.1

### How do I get set up? ###

The package depends on standard ros packages

### Who do I talk to? ###

Diego Pardo (depardo@ethz.ch)
