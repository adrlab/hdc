/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * data_map.hpp
 *
 *  Created on: Aprl 24
 *      Author: Diego Pardo
 *      Based on the original idea and version from IIT
 *      IIT HYQ COMMONS LEGDATAMAP
 *      This class generalizes such concept to any robot and End Effector
 */

#ifndef _DATAMAP_H_
#define _DATAMAP_H_

#include <Eigen/Dense>
#include <stdexcept>
#include <iostream>
#include <cstring>
#include <Eigen/StdVector>
#include <array>


/**
 * A very simple container to N datatypes
 */
template<typename T, size_t N>
	class DataMap {

	public:

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	protected:
	    T data[N];
	public:
	    DataMap() {};
	    DataMap(const T& defaultValue);
	    DataMap(const T* defaultarray);
	    DataMap(const DataMap<T,N>& rhs);
	    //DataMap(const T defaultValue);
	    DataMap<T,N>& operator=(const DataMap<T,N>& rhs);
	    DataMap<T,N>& operator=(const T& defaultValue);
	    T& operator[](const int & index);
	    //T& operator[](int index) throw(std::runtime_error);
	    const T& operator[](const int & index) const;
	    //const T& operator[](int index) const throw(std::runtime_error);

	    std::vector<T> ToVector() const;
	    std::array<T,N> ToArray() const;
	    size_t size(){return (static_cast<size_t>(N));}
	private:
	    void copydata(const DataMap<T,N>& rhs);
	    void assignAll(const T& value);
	};

template<typename T, size_t N> inline
DataMap<T,N>::DataMap(const T* defaultarray) {
	std::memcpy(data,defaultarray,N*sizeof(T));
}
template<typename T, size_t N> inline
DataMap<T,N>::DataMap(const T& defaultValue) {
    assignAll(defaultValue);
}

template<typename T, size_t N> inline
DataMap<T,N>::DataMap(const DataMap<T,N>& rhs) //cannot use initializer list for arrays?
{
    copydata(rhs);
}

template<typename T , size_t N> inline
DataMap<T,N>& DataMap<T,N>::operator=(const DataMap<T,N>& rhs)
{
    if(&rhs != this) {
        copydata(rhs);
    }
    return *this;
}

template<typename T, size_t N> inline
DataMap<T,N>& DataMap<T,N>::operator=(const T& defaultValue)
{
    assignAll(defaultValue);
    return *this;
}

template<typename T, size_t N> inline
T& DataMap<T,N>::operator[](const int & index) {
  if(index < 0 || index > N - 1)
    throw(std::runtime_error("DATA_MAP: Index out of bounds"));
  else
    return data[index];
}


template<typename T,size_t N> inline
const T& DataMap<T,N>::operator[](const int & index ) const {
  if(index < 0 || index > N - 1)
    throw(std::runtime_error("DATA_MAP: Index out of bounds"));
  else
    return data[index];
}

template<typename T, size_t N> inline
std::vector<T> DataMap<T,N>::ToVector() const
{

  std::vector<T> vec(N);
  for(int i = 0 ; i < N ; i++) {
    vec[i] = data[i];
  }

  return vec;
}

template<typename T, size_t N> inline
std::array<T,N> DataMap<T,N>::ToArray() const
{
  std::array<T,N> array;
  int i=0;
  for (auto& a : array)
    a = data[i++];

  return array;
}

template<typename T, size_t N> inline
void DataMap<T,N>::copydata(const DataMap<T,N>& rhs) {

  for(int i = 0 ; i < N ; i++) {
    data[i] = rhs[i];
  }
}

template<typename T, size_t N> inline
void  DataMap<T,N>::assignAll(const T& value) {

  for(int i = 0 ; i < N ; i++) {
    data[i] = value;
  }

}

template<typename T, size_t N> inline
std::ostream& operator<<(std::ostream& out, const DataMap<T,N>& map) {
  for(int i = 0 ; i < N ; i++) {
    out << "[" << i << "]= " << map[i] << std::endl;
  }
  return out;
}

#endif // _DATAMAP_H_
