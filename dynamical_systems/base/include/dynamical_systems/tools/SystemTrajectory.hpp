/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * SystemTrajectory.hpp
 *
 *  Created on: May 3, 2017
 *      Author: depardo
 */

#ifndef _SYSTTRAJECTORY_HPP_
#define _SYSTTRAJECTORY_HPP_

#include <Eigen/Dense>
#include <dynamical_systems/base/Dimensions.hpp>
#include <dynamical_systems/base/DynamicsBase.hpp>
#include <dynamical_systems/tools/LRConversions.hpp>


template <class DIMENSIONS>
class SystemTrajectory : public LRConversions<DIMENSIONS> {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef typename DIMENSIONS::state_vector_t state_vector_t;
  typedef typename DIMENSIONS::control_vector_t control_vector_t;
  typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
  typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;
  typedef typename DIMENSIONS::GeneralizedCoordinates_t GeneralizedCoordinates_t;
  typedef typename DIMENSIONS::GeneralizedCoordinates_array_t GeneralizedCoordinates_array_t;

  static const int n_dof = DIMENSIONS::DimensionsSize::DOF_SIZE;

  typedef std::vector<double> trajectory_vector_t;
  typedef std::vector<double> increments_vector_t;

  SystemTrajectory(const state_vector_array_t & x_traj, const control_vector_array_t & u_traj,
                   const Eigen::VectorXd & times);
  SystemTrajectory(){};

  ~SystemTrajectory(){};

  size_t trajectory_size = 0;
  state_vector_array_t x_t;
  state_vector_array_t xd_t;
  control_vector_array_t u_t;
  GeneralizedCoordinates_array_t q_t;
  GeneralizedCoordinates_array_t qd_t;
  GeneralizedCoordinates_array_t qdd_t;

  Eigen::VectorXd t_;
  Eigen::VectorXd h_;
  trajectory_vector_t t_vector;
  increments_vector_t h_vector;

  bool trajectory_is_ok = false;

  // optional
  bool derivatives_trajectories_set = false;

  bool updateTrajectory(const state_vector_array_t & x , const control_vector_array_t & u ,
                        const Eigen::VectorXd & times);
  void updateStates(const state_vector_array_t & x_update_t);
  void updateControls(const control_vector_array_t & u_update_t);
  void updateIncrements(const Eigen::VectorXd &h_increments);
  void updateTimes(const Eigen::VectorXd & t);
  void add_xut_point(const state_vector_t & x, const control_vector_t & u, const double & t_number );
  void add_xd_point(const state_vector_t & xd);

  bool setStateDerivatives(const state_vector_array_t & xd);
  bool setDOFDerivatives();
  bool getTajectoryDerivatives(state_vector_array_t & xd) {
    bool flag = false;
    if(derivatives_trajectories_set) {
        xd = xd_t;
        flag = true;
    }
    return flag;
  }

  bool getTrajectoryDerivatives(GeneralizedCoordinates_array_t & q_derivative, GeneralizedCoordinates_array_t & qdd ) {
    bool flag = false;
    if(derivatives_trajectories_set) {
        q_derivative = q_derivative_t;
        qdd = qdd_t;
        flag = true;
    }
    return flag;
  }

  void clearTrajectory();

private:
  void updateGeneralizedCoordinates();
  std::vector<double> getVector(const Eigen::VectorXd & dv);
  Eigen::VectorXd getEigen(const std::vector<double> & d_vector);
  Eigen::VectorXd t2h(const Eigen::VectorXd & t_);
  GeneralizedCoordinates_array_t q_derivative_t;

};


template <class DIMENSIONS>
SystemTrajectory<DIMENSIONS>::SystemTrajectory(const state_vector_array_t & x_traj , const control_vector_array_t & u_traj,
                                   const Eigen::VectorXd & times) {

  updateTrajectory(x_traj,u_traj,times);

}

template <class DIMENSIONS>
bool SystemTrajectory<DIMENSIONS>::updateTrajectory(const state_vector_array_t & x_traj , const control_vector_array_t & u_traj ,
                                              const Eigen::VectorXd & times) {

  trajectory_size = x_traj.size();
  t_.resize(trajectory_size);
  h_.resize(trajectory_size-1);

  trajectory_is_ok = true;

  updateStates(x_traj);
  updateControls(u_traj);

  if(times.size() == trajectory_size) {

      updateTimes(times);

  } else if(times.size() == trajectory_size - 1 ) {

      updateIncrements(times);
  } else {
    trajectory_is_ok = false;
  }

  return trajectory_is_ok;

}

template <class DIMENSIONS>
void SystemTrajectory<DIMENSIONS>::add_xut_point(const state_vector_t & x, const control_vector_t & u, const double & t_number ) {
 x_t.push_back(x);
 u_t.push_back(u);
 trajectory_size++;
 t_vector.push_back(t_number);
 updateTimes(getEigen(t_vector));

 //override stdvector version of times
 t_vector = getVector(t_);
 h_vector = getVector(h_);

 updateGeneralizedCoordinates();

}

template <class DIMENSIONS>
void SystemTrajectory<DIMENSIONS>::add_xd_point(const state_vector_t & xd) {
  //only valid if there is exactly one point missing
  if(x_t.size() != trajectory_size - 1)
  xd_t.push_back(xd);
}

template <class DIMENSIONS>
std::vector<double> SystemTrajectory<DIMENSIONS>::getVector(const Eigen::VectorXd & dv) {

  int v_size = dv.size();
  std::vector<double> d_vector;

  for(int k = 0 ; k < v_size ; ++k) {
      d_vector.push_back(dv(k));
  }

  return d_vector;
}

template <class DIMENSIONS>
Eigen::VectorXd SystemTrajectory<DIMENSIONS>::getEigen(const std::vector<double> & d_vector) {

  int v_size = d_vector.size();
  Eigen::VectorXd dv(v_size);

  for(int k = 0 ; k < v_size ; ++k) {
      dv(k) = d_vector[k];
  }

  return dv;
}

template <class DIMENSIONS>
void SystemTrajectory<DIMENSIONS>::updateStates(const state_vector_array_t & x_update_t){
  if(x_update_t.size() != trajectory_size) {
      std::cout << "System Trajectory Error: Error Updating States" << std::endl;
      std::exit(EXIT_FAILURE);
  } else {
      x_t = x_update_t;
  }
  updateGeneralizedCoordinates();
}

template <class DIMENSIONS>
void SystemTrajectory<DIMENSIONS>::updateControls(const control_vector_array_t & u_update_t){
  if(u_update_t.size() != trajectory_size) {
      std::cout << "System Trajectory Error: Error Updating Controls" << std::endl;
      std::exit(EXIT_FAILURE);
  } else {
      u_t = u_update_t;
  }
}

template <class DIMENSIONS>
void SystemTrajectory<DIMENSIONS>::updateIncrements(const Eigen::VectorXd & h_increments) {

  if(h_increments.size() != trajectory_size - 1) {
      std::cout << "System Trajectory Error: Error Updating Increments" << std::endl;
      std::exit(EXIT_FAILURE);
  }

  h_ = h_increments;
  t_(0) = 0;
  t_.tail(trajectory_size-1) = h_;
  for(int k=1; k < trajectory_size-1; ++k) {
      t_(k) += t_(k-1);
  }

  //override std vector version
  t_vector = getVector(t_);
  h_vector = getVector(h_);
}

template <class DIMENSIONS>
void SystemTrajectory<DIMENSIONS>::updateTimes(const Eigen::VectorXd & t_t){
  if(t_t.size() != trajectory_size) {
      std::cout << "System Trajectory Error: Error Updating Time" << std::endl;
      std::cout << "your time size: " << t_t.size() << std::endl;
      std::cout << "trajectory_size: " << trajectory_size << std::endl;
      std::exit(EXIT_FAILURE);
  }

  t_ = t_t;
  h_ = t_.tail(trajectory_size-1) - t_.head(trajectory_size-1);

  //validate monotonically increasing time
  for(int k = 1 ; k < trajectory_size; ++k) {
      if(t_(k) < t_(k-1)) {
          std::cout << "System Trajectory Error: Invalid times" << std::endl;
          std::cout << t_.transpose() << std::endl;
          std::exit(EXIT_FAILURE);
      }
  }

  t_vector = getVector(t_);
  h_vector = getVector(h_);
}

template <class DIMENSIONS>
void SystemTrajectory<DIMENSIONS>::updateGeneralizedCoordinates() {

  GeneralizedCoordinates_t q,qd;
  q_t.clear();
  qd_t.clear();

  for(int k = 0 ; k < trajectory_size ; ++k) {
      q  = x_t[k].template segment<n_dof>(0);
      qd = x_t[k].template segment<n_dof>(n_dof);

      q_t.push_back(q);
      qd_t.push_back(qd);
  }


}

template <class DIMENSIONS>
bool SystemTrajectory<DIMENSIONS>::setStateDerivatives(const state_vector_array_t &xd) {

  if(!trajectory_is_ok || xd.size() != trajectory_size) {
      return false;
  }

  xd_t.clear();
  xd_t = xd;
  derivatives_trajectories_set = true;
  setDOFDerivatives();

  return true;
}

template <class DIMENSIONS>
bool SystemTrajectory<DIMENSIONS>::setDOFDerivatives() {

  if(!derivatives_trajectories_set || xd_t.size()!=trajectory_size){
      return false;
  }

  updateGeneralizedCoordinates();

  q_derivative_t.clear();
  qdd_t.clear();

  for(int k = 0; k < trajectory_size ; ++k) {

      q_derivative_t.push_back(xd_t[k].template segment<n_dof>(0));
      qdd_t.push_back(xd_t[k].template segment<n_dof>(n_dof));
  }
  return true;

}

template <class DIMENSIONS>
void SystemTrajectory<DIMENSIONS>::clearTrajectory() {

   x_t.clear();
   u_t.clear();
   //t_.resize(0);
   trajectory_is_ok = false;

   q_t.clear();
   qd_t.clear();

   xd_t.clear();
   q_derivative_t.clear();
   qdd_t.clear();
   derivatives_trajectories_set = false;
}

template <class DIMENSIONS>
const int SystemTrajectory<DIMENSIONS>::n_dof;

#endif /* _SYSTTRAJECTORY_HPP_ */
