/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file     LeggedRobotDynamics.hpp
 *  \brief    Base class for a legged robot
 *  \authors  depardo
 *  \date     Apr 24, 2017
 */

#ifndef _LEGGEDROBOTDYNAMICS_HPP_
#define _LEGGEDROBOTDYNAMICS_HPP_

#include <Eigen/Dense>
#include <memory>
#include <dynamical_systems/base/Dimensions.hpp>
#include <dynamical_systems/base/DynamicsBase.hpp>
#include <dynamical_systems/base/extras/GRFBase.hpp>


template <class DIMENSIONS>
class LeggedRobotDynamics:public DynamicsBase<DIMENSIONS> {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef typename DIMENSIONS::state_vector_t state_vector_t;
  typedef typename DIMENSIONS::control_vector_t control_vector_t;

  typedef typename DIMENSIONS::InertiaMatrix_t InertiaMatrix_t;

  typedef typename DIMENSIONS::ContactConfiguration_t ContactConfiguration_t;
  typedef typename DIMENSIONS::GeneralizedCoordinates_t GeneralizedCoordinates_t;
  typedef typename DIMENSIONS::EEPositionsDataMap_t EEPositionsDataMap_t;
  typedef typename DIMENSIONS::EEJacobianTranspose_t EEJacobianTranspose_t;
  typedef typename DIMENSIONS::EEJacobian_t EEJacobian_t;

  static const int nee = static_cast<int>(DIMENSIONS::DimensionsSize::EE_SIZE);

  LeggedRobotDynamics(std::shared_ptr<GRFBase<DIMENSIONS>> grf):grf_(grf){

    ContactConfiguration_t fc(false);
    n_ee_contact = 0;
    ee_contact_id.clear();
    fc_= fc;
    c_code_ = 0;
    changeRobotFeetConfiguration(fc);
  }

  LeggedRobotDynamics(const ContactConfiguration_t & fc,std::shared_ptr<GRFBase<DIMENSIONS>> grf):grf_(grf){
    setContactConfiguration(fc);
  }

  void setContactConfiguration(const ContactConfiguration_t & fc);
  bool setContactConfiguration(const int & c_code);
  void getContactConfigurationFromCode(const int & c_code, ContactConfiguration_t & fc);

  ContactConfiguration_t getContactConfiguration(){ return fc_; }
  int getContactConfigurationCode(){ return c_code_; }

  virtual void getEEPose(const GeneralizedCoordinates_t& big_q, EEPositionsDataMap_t & EEPose_inertia,
                         EEPositionsDataMap_t & EEPose_base) = 0;

  virtual EEPositionsDataMap_t getEEPoseWorld() = 0;

  int getNumberPointsContact() {
    return n_ee_contact;
  }
  std::vector<int> getContactPointIDVector() {
    return ee_contact_id;
  }

  virtual void changeRobotFeetConfiguration(const ContactConfiguration_t & fc) {};

  virtual void getCoordinatesFromState(const state_vector_t & x1,
                                       GeneralizedCoordinates_t & q , GeneralizedCoordinates_t & qd) = 0;
  virtual void updateState(const state_vector_t & x) = 0;
  virtual void updateContactForces(const control_vector_t & u) = 0;
  virtual void updateContactForces(const state_vector_t & x , const control_vector_t & u) {
    updateState(x);
    updateContactForces(u);
  }
  virtual void updateContactForcesQuick(const GeneralizedCoordinates_t & tau_minus_h_minus_Mqdd) = 0;
  virtual void getInertiaMatrix(const GeneralizedCoordinates_t & q , InertiaMatrix_t & M) = 0;
  virtual void getOrthogonalProjection(InertiaMatrix_t & P , EEJacobianTranspose_t & Jinv) = 0;
  virtual GeneralizedCoordinates_t& getTauMinusHminusMqdd() = 0;
  virtual Eigen::Affine3d & getWXBTransform() = 0;
  virtual EEJacobian_t getJc() = 0;

  // user should make sure this is pointing to the Robot GRF
  std::shared_ptr<GRFBase<DIMENSIONS>> grf_;

  virtual ~LeggedRobotDynamics(){}

private:

  std::vector<int> ee_contact_id; /* vector with the id of the ee in contact */
  int n_ee_contact = 0;           /* number of ee in contact */
  int c_code_ = 0;                /* int id of the contact configuration */
  ContactConfiguration_t fc_;     /* true/false codification of the contact configuration*/
};

template <class DIMENSIONS>
bool LeggedRobotDynamics<DIMENSIONS>::setContactConfiguration(const int & c_code) {

  ContactConfiguration_t fc = DIMENSIONS::get_all_contact_configurations()[c_code];
  setContactConfiguration(fc);

  return true;
}

template <class DIMENSIONS>
void LeggedRobotDynamics<DIMENSIONS>::getContactConfigurationFromCode(const int & c_code, ContactConfiguration_t & fc) {

  int bits_in_code = nee;

  Eigen::VectorXd table_(bits_in_code);
  int max_number = (int)std::pow(2,bits_in_code);
  if(c_code > max_number){
      std::cout << "error decoding feet configuration" << std::endl;
      std::exit(EXIT_FAILURE);
      return;
  }

  double rem = (double) c_code;

  for(int p = bits_in_code-1 ; p >= 0 ; --p) {
      table_(p) = std::pow(2,p);
      if(rem >= table_(p)) {
         fc[p] = true;
         rem = rem - table_(p);
      }
      else {
          fc[p] = false;
      }
  }
}

template <class DIMENSIONS>
void LeggedRobotDynamics<DIMENSIONS>::setContactConfiguration(const ContactConfiguration_t & fc) {

  n_ee_contact = 0;
  c_code_ = 0;
  ee_contact_id.clear();
  for(int i = 0 ; i < nee ; i++) {
      if(fc[i]){
        n_ee_contact++;
        ee_contact_id.push_back(i);
        c_code_ += std::pow(2,i);
      }
  }
  fc_ = fc;
  if(grf_!=NULL)
    grf_->changeFeetConfiguration(fc_);
  changeRobotFeetConfiguration(fc_);
}
#endif /* _LEGGEDROBOTDYNAMICS_HPP_ */
