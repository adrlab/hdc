/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * PFLState.hpp
 *
 *  Created on: August 04,2017
 *      Author: depardo
 */

#ifndef _PFLState_HPP_
#define _PFLState_HPP_

#include <memory>

#include <Eigen/Dense>

#include <dynamical_systems/systems/pointFootLeg/PFLDimensions.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLConstraintJacobian.hpp>

class PFLState : public PFLConstraintJacobian {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef robotDimensions::state_vector_t state_vector_t;
  typedef robotDimensions::control_vector_t control_vector_t;
  typedef robotDimensions::ContactConfiguration_t ContactConfiguration_t;
  typedef robotDimensions::GeneralizedCoordinates_t GeneralizedCoordinates_t;
  typedef robotDimensions::BaseCoordinates_t BaseCoordinates_t;
  typedef robotDimensions::JointCoordinates_t JointCoordinates_t;
  typedef robotDimensions::EELambda_t EELambda_t;

  /**
   * @brief Constructor given a contact configuration and the whole-body configuration and velocity
   * @param fc the DataMap with the current EE contact configuration
   * @param q_robot GeneralizedCoordinates vector with the whole-body configuration
   * @param qdot_robot GeneralizedCoordinates vector with the whole-body velocity
   */
 	PFLState(const ContactConfiguration_t & fc, const GeneralizedCoordinates_t & q_robot,
			const GeneralizedCoordinates_t & qdot_robot);

  /**
   * @brief Constructor given the contact configuration and assuming default configuration (zero velocity)
   * @param fc the DataMap with the current EE contact configuration
   */
	PFLState(const ContactConfiguration_t & fc);

	/**
	 *
	 */
	PFLState();

	~PFLState();

	PFLState& operator=(const PFLState& rhs);

	///Robot State

	int number_of_feet_in_contact = 0;
	std::vector<int> contact_leg_number;

	GeneralizedCoordinates_t big_q;
	GeneralizedCoordinates_t big_qdot;
	GeneralizedCoordinates_t q_derivative;
  JointCoordinates_t joints_pos;
  JointCoordinates_t joints_vel;
  state_vector_t x_state;

	BaseCoordinates_t base_position_world;
	BaseCoordinates_t base_velocity_base;
	BaseCoordinates_t base_velocity_world;
	Eigen::Vector3d r_base;

	Eigen::Affine3d W_X_B;
	Eigen::Matrix3d W_R_B;
	Eigen::Matrix3d B_R_W;

	EEPositionsDataMap_t Feet_Base;
	EEPositionsDataMap_t Feet_World;
  EELambda_t feet_velocity;

	Eigen::Vector3d gravity_world_;
	Eigen::Vector3d gravity_base_;

	/**
	 * update the state of the robot with new q and qd
	 * @param q_robot the GeneraliedCoordinate vector with the position of the robot
	 * @param qd_robot the GeneralizedCoordinate vector with the velocitiy of the robot
	 */
	void update(const GeneralizedCoordinates_t & q_robot,
			const GeneralizedCoordinates_t & qd_robot);

	/**
	 * update the state of the robot with new state
	 * @param x the state vector
	 */
	void updateState(const state_vector_t & x);

	/**
	 * @brief change the contact configuration
	 * @param fc the DataMap with the contact configuration
	 */
	void changecontactconfiguration(const ContactConfiguration_t &fc);

  /**
   * @brief update all the variables of the state with the current q, qd (i.e., state)
   * and contact configuration
   */
  void updateRobot();


	void GetCurrentRealWFeetVelocity(EEPositionsDataMap_t & fv);
	void EulerAngleRateOfChange(const Eigen::Vector3d & w , Eigen::Vector3d & dedt);

private:

	inline void copydata(const PFLState& rhs);
	void SetPositionDerivative();
  void SetNumberOfContacts(const ContactConfiguration_t &fc);
	void initializeGravity();
};
#endif /* _PFLState_HPP_ */
