/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file 		PFLProjectedDynamics.hpp
 *	\brief		Class providing Direct/Inverse Dynamics in the constraint consistent subspace
 *  \authors    depardo
 */

#ifndef PFLProjectedDynamics_HPP_
#define PFLProjectedDynamics_HPP_

#include <memory>

#include <Eigen/Dense>

#include <dynamical_systems/base/LeggedRobotDynamics.hpp>

#include <dynamical_systems/systems/pointFootLeg/iit/inertia_properties.h>
#include <dynamical_systems/systems/pointFootLeg/iit/jsim.h>
#include <dynamical_systems/systems/pointFootLeg/iit/inverse_dynamics.h>

#include <dynamical_systems/systems/pointFootLeg/PFLDimensions.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLState.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLKinematics.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLConstraintJacobian.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLJacobianDerivative.hpp>

/**
 * @brief This class provides the constraint consistent equations of motion of HyQ
 */
class PFLProjectedDynamics : public LeggedRobotDynamics<robotDimensions> {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef robotDimensions::state_vector_t state_vector_t;
	typedef robotDimensions::control_vector_t control_vector_t;
	typedef robotDimensions::state_vector_array_t state_vector_array_t;
	typedef robotDimensions::control_vector_array_t control_vector_array_t;
	typedef robotDimensions::control_feedback_t control_feedback_t;
	typedef robotDimensions::control_feedback_array_t control_feedback_array_t;
	typedef robotDimensions::ContactConfiguration_t ContactConfiguration_t;
	typedef robotDimensions::GeneralizedCoordinates_t GeneralizedCoordinates_t;
	typedef robotDimensions::BaseCoordinates_t BaseCoordinates_t;
	typedef robotDimensions::JointCoordinates_t JointCoordinates_t;
	typedef robotDimensions::EELambda_t EELambda_t;
	typedef robotDimensions::EEJacobianTranspose_t EEJacobianTranspose_t;
	typedef robotDimensions::EEJacobian_t EEJacobian_t;
	typedef robotDimensions::InertiaMatrix_t InertiaMatrix_t;
	typedef robotDimensions::JointMatrix_t JointMatrix_t;
	typedef robotDimensions::BaseJointMatrix_t BaseJointMatrix_t;
	typedef robotDimensions::TorqueCommand_t TorqueCommand_t;

	static const int kBaseDof     = robotDimensions::kBaseDof;
	static const int kTotalEE     = robotDimensions::kTotalEE;
	static const int kTotalDof    = robotDimensions::kTotalDof;
	static const int kTotalJoints = robotDimensions::kTotalJoints;

	static const int qr0 = robotDimensions::CoordinatesOrder::qr0;
	static const int qb0 = robotDimensions::CoordinatesOrder::qb0;

public:

	/**
	 * @brief Create a new instance of the constraint consistent hynamics of hyq.
	 * @param[in] feet_contact The LegDataMap of booleans indicating feet in contact.
	 */
	PFLProjectedDynamics(const ContactConfiguration_t & feet_contact);

	/**
	 * @brief Destructs the PFLProjectedDynamics instance
	 */
	virtual ~PFLProjectedDynamics() { };

	/**
	 * @brief Main interface to obtain the Direct Dynamics of the projected dynamics of HyQ
	 */
	virtual state_vector_t systemDynamics(const state_vector_t & x , const control_vector_t & u) override;

	/**
	 * @brief Computes the Null space projector operator for the robot at the current configuration
	 * @param[out] P Null space projector operator of the column space of the Jacobian of the constraints
	 * @param[out] Jcinv Moore-Penrose pseudo inverse of the Jacobian of the constraints.
	 */
	void getOrthogonalProjection(InertiaMatrix_t & P, EEJacobianTranspose_t & Jcinv);

	/**
	 * @brief Verifies that the input matrix verifies the conditions of a null space projector
	 * @param[out] P Null space projector operator of the column space of the Jacobian of the constraints
	 */
	bool VerifyProjection(const Eigen::MatrixXd & P);

	/**
	 * @brief Computes the constrained and unconstrained basis of the Orthogonal matrix Q
	 * @param[in] Q The orthogonal basis-matrix of the transpose of the Jacobian of the constraints
	 * @param[out] Qc The orthogonal basis-matrix of the constraint subspace
	 * @param[out] Qu The orthogonal basis-matrix of the unconstraint subspace
	 * @param[in] rank The rank of Q
	 */
	void GetConstrainedUnconstrainedQ(const Eigen::MatrixXd & Q,
			Eigen::MatrixXd & Qc, Eigen::MatrixXd & Qu , const int & rank);

	/**
	 * @brief Computes the vector of generalized forces for certain HyQState
	 * @param[in] h_s The HyQState
	 * @param[out] F The generalized forces (configuration space)
	 */
	void GeneralizedContactForces(const PFLState & h_s, GeneralizedCoordinates_t & F );

	/**
	 * @brief Computes the position time derivative of a given state
	 * @param[in] x The state of hyq (in Plooker Coordinates)
	 * @param[out] qprime The time derivative of the hyq position
	 */
	void positionStateDerivative(const state_vector_t &x, GeneralizedCoordinates_t & qprime);

	/**
	 * @brief Gets the current feet configuration
	 * @param[out] current_feet_config The current contact-feet configuration
	 */
	void GetFeetConfiguration( ContactConfiguration_t & current_feet_config) {
		current_feet_config = pfl_state.getFeetConfiguration(); }

	/**
	 * @brief Changes the current feet configuration
	 * @param fc
	 */
	void setFeetConfiguration(const ContactConfiguration_t & fc );
	void changeRobotFeetConfiguration(const ContactConfiguration_t & fc ) {
	  setFeetConfiguration(fc);
	}

	/**
	 * @brief Gets the Whole-Body Inertia Matrix for the current state of the robot
	 * @param[out] bigM The Whole-Body Inertia Matrix
	 */
	void getMfromDynamics(iit::pfLeg::dyn::JSIM::MatrixType & bigM);

	void getInertiaMatrix(const GeneralizedCoordinates_t & big_q, InertiaMatrix_t & M) override;

	/**
	 * @brief Gets Independent blocks from the Whole-Body Inertia Matrix at the current state of the robot
	 * @param[in] big_q The robot configuration
	 * @param[out] F Base Inertia Matrix
	 * @param[out] H Joints Inertia Matrix
	 */
	void GetFHfromDynamics(const GeneralizedCoordinates_t & big_q, BaseJointMatrix_t & F, JointMatrix_t & H);

	/**
	 * @brief Gets the vector of
	 * @param[out] h_vector The generalized force vector containing the Coriolis, centrifugal and gravitational effects
	 */
	void GetCandGfromDynamics(GeneralizedCoordinates_t & h_vector);

	/**
	 * @brief Compute the ground reaction forces for the current state of the robot and a given control vector
	 * @param[in] u The generalized force vector containing the Coriolis, centrifugal and gravitational effects
	 */
	void GetContactForces(const control_vector_t & u );

	/**
	 * @brief Update the ground reaction forces member
	 * @param[in] tau_minus_h_minus_Mqdd vector
	 */
	void UpdateGRF(const GeneralizedCoordinates_t & tau_minus_h_minus_Mqdd);

	/**
	 * @brief Constraint consistent accelerations of HyQ for the current state of the robot
	 * @param[in] tau control vector
	 * @param[out] tau_minus_h_Mqdd vector
	 * @return Vector of constraint consistent accelerations
	 */
	GeneralizedCoordinates_t systemCDynamics(const control_vector_t & tau,
	                                         GeneralizedCoordinates_t & tau_minus_h_Mqdd);

	/**
	 * @brief Constraint consistent inverse dynamics
	 * @param[in] qdd_desired desired accelerations
	 * @param[out] inverse_dynamics_u The torque commands to achieve the desired accelerations
	 */
	void InverseDynamicsControl(const GeneralizedCoordinates_t & qdd_desired,
	                            TorqueCommand_t & inverse_dynamics_u);


  virtual void getEEPose(const GeneralizedCoordinates_t& big_q, EEPositionsDataMap_t & EEPose_inertia,
                         EEPositionsDataMap_t & EEPose_base) override {
    pfl_state.GetFeetPose(big_q,EEPose_inertia,EEPose_base);
  }

  virtual EEPositionsDataMap_t getEEPoseWorld() override {

    return (pfl_state.Feet_World);
  }
  virtual void getCoordinatesFromState(const state_vector_t & x1,
                                       GeneralizedCoordinates_t & q , GeneralizedCoordinates_t & qd) override {
    pfl_state.GeneralizedCoordinatesFromStateVector(x1,q,qd);
  }
  virtual void updateContactForcesQuick(const GeneralizedCoordinates_t & tau_minus_h_minus_Mqdd) override {
    UpdateGRF(tau_minus_h_minus_Mqdd);
  }

  virtual GeneralizedCoordinates_t& getTauMinusHminusMqdd() override {
    return (this->_tau_minus_h_minus_Mqdd);
  }
  virtual Eigen::Affine3d & getWXBTransform() override {
    return (pfl_state.W_X_B);
  }
  virtual EEJacobian_t getJc() override {
    return (pfl_state.GetJc());
  }
  virtual void updateContactForces(const control_vector_t & u) override {
    GetContactForces(u);
  }
  virtual void updateState(const state_vector_t & x) override {
    pfl_state.updateState(x);
  }


	std::unique_ptr<PFLJacobianDerivative> pfl_JacobianDerivative_;
	Eigen::FullPivLU<InertiaMatrix_t> fplu;


	GeneralizedCoordinates_t global_F;

	PFLState pfl_state; /*!<PFL State */
	EEJacobian_t _Jdot;

  Eigen::Matrix<double,kTotalJoints, kTotalDof> torque_slection_matrix_S;

//private:
  iit::pfLeg::dyn::InertiaProperties inertia_properties;
  iit::pfLeg::ForceTransforms base_forceTransforms;
	iit::pfLeg::MotionTransforms base_motionTransforms;
  iit::pfLeg::dyn::JSIM inertia_matrix;
	iit::pfLeg::dyn::InverseDynamics inverse_dynamics;

	Eigen::MatrixXd selection_matrix_Su;

	InertiaMatrix_t P_,M_;

	GeneralizedCoordinates_t h_, qdd_;
  EEJacobianTranspose_t Jinv_;
  GeneralizedCoordinates_t b_;
  GeneralizedCoordinates_t tau_minus_h_;
  GeneralizedCoordinates_t _tau_minus_h_minus_Mqdd;
  InertiaMatrix_t PM_;
  InertiaMatrix_t PMT_;
  InertiaMatrix_t Mtilda_;

  PFLKinematics pfl_kin; /*!< HyQ kinematics tools */

};

#endif /* PFLProjectedDynamics_HPP_ */
