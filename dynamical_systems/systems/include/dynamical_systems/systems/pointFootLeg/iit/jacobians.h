#ifndef PFLEG_JACOBIANS_H_
#define PFLEG_JACOBIANS_H_

		#include <iit/rbd/rbd.h>
#include <iit/rbd/TransformsBase.h>
#include <iit/rbd/traits/DoubleTrait.h>
#include "declarations.h"
#include "kinematics_parameters.h"

namespace iit {
namespace pfLeg {

template<typename SCALAR, int COLS, class M>
class JacobianT : public iit::rbd::JacobianBase<tpl::JointState<SCALAR>, COLS, M>
{};

namespace tpl {

/**
 *
 */
template <typename TRAIT>
class Jacobians {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        typedef typename TRAIT::Scalar Scalar;
        typedef iit::rbd::Core<Scalar> CoreS;

        typedef JointState<Scalar> JState;

        class Type_fr_trunk_J_LF_hipassemblyCOM : public JacobianT<Scalar, 1, Type_fr_trunk_J_LF_hipassemblyCOM>
        {
        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            Type_fr_trunk_J_LF_hipassemblyCOM();
            const Type_fr_trunk_J_LF_hipassemblyCOM& update(const JState&);
        protected:
        };
        
        class Type_fr_trunk_J_LF_upperlegCOM : public JacobianT<Scalar, 2, Type_fr_trunk_J_LF_upperlegCOM>
        {
        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            Type_fr_trunk_J_LF_upperlegCOM();
            const Type_fr_trunk_J_LF_upperlegCOM& update(const JState&);
        protected:
        };
        
        class Type_fr_trunk_J_LF_lowerlegCOM : public JacobianT<Scalar, 3, Type_fr_trunk_J_LF_lowerlegCOM>
        {
        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            Type_fr_trunk_J_LF_lowerlegCOM();
            const Type_fr_trunk_J_LF_lowerlegCOM& update(const JState&);
        protected:
        };
        
        class Type_fr_trunk_J_LF_foot : public JacobianT<Scalar, 3, Type_fr_trunk_J_LF_foot>
        {
        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            Type_fr_trunk_J_LF_foot();
            const Type_fr_trunk_J_LF_foot& update(const JState&);
        protected:
        };
        
    public:
        Jacobians();
        void updateParameters();
    public:
        Type_fr_trunk_J_LF_hipassemblyCOM fr_trunk_J_LF_hipassemblyCOM;
        Type_fr_trunk_J_LF_upperlegCOM fr_trunk_J_LF_upperlegCOM;
        Type_fr_trunk_J_LF_lowerlegCOM fr_trunk_J_LF_lowerlegCOM;
        Type_fr_trunk_J_LF_foot fr_trunk_J_LF_foot;

    protected:

};

} //namespace tpl

using Jacobians = tpl::Jacobians<rbd::DoubleTrait>;

#include "jacobians.impl.h"


}
}

#endif
