#ifndef IIT_ROBOGEN__PFLEG_TRAITS_H_
#define IIT_ROBOGEN__PFLEG_TRAITS_H_

#include "declarations.h"
#include "transforms.h"
#include "inverse_dynamics.h"
#include "forward_dynamics.h"
#include "jsim.h"
#include "inertia_properties.h"
#include "jacobians.h"
#include <iit/rbd/traits/TraitSelector.h>


namespace iit {
namespace pfLeg {

namespace tpl {

template <typename SCALAR>
struct Traits {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    typedef SCALAR S;

    typedef typename pfLeg::JointIdentifiers JointID;
    typedef typename pfLeg::LinkIdentifiers  LinkID;
    typedef typename iit::rbd::tpl::TraitSelector<SCALAR>::Trait Trait;

    typedef typename pfLeg::tpl::JointState<SCALAR> JointState;



    typedef typename pfLeg::tpl::HomogeneousTransforms<Trait> HomogeneousTransforms;
    typedef typename pfLeg::tpl::MotionTransforms<Trait> MotionTransforms;
    typedef typename pfLeg::tpl::ForceTransforms<Trait> ForceTransforms;
    typedef typename pfLeg::tpl::Jacobians<Trait> Jacobians;

    typedef typename iit::pfLeg::dyn::tpl::InertiaProperties<Trait> InertiaProperties;
    typedef typename iit::pfLeg::dyn::tpl::ForwardDynamics<Trait> FwdDynEngine;
    typedef typename iit::pfLeg::dyn::tpl::InverseDynamics<Trait> InvDynEngine;
    typedef typename iit::pfLeg::dyn::tpl::JSIM<Trait> JSIM;

    static const int joints_count = pfLeg::jointsCount;
    static const int links_count  = pfLeg::linksCount;
    static const bool floating_base = true;

    static inline const JointID* orderedJointIDs();
    static inline const LinkID*  orderedLinkIDs();
};

template <typename SCALAR>
inline const typename Traits<SCALAR>::JointID*  Traits<SCALAR>::orderedJointIDs() {
    return pfLeg::orderedJointIDs;
}
template <typename SCALAR>
inline const typename Traits<SCALAR>::LinkID*  Traits<SCALAR>::orderedLinkIDs() {
    return pfLeg::orderedLinkIDs;
}

}

typedef tpl::Traits<double> Traits; // default instantiation - backward compatibility...

}
}

#endif
