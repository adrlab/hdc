/*
 * PFLExtendedJacobian.cpp
 *
 *  Created on: August 4, 2017
 *      Author: depardo
 */

#include <dynamical_systems/systems/pointFootLeg/PFLExtendedJacobians.hpp>

PFLExtendedJacobians::PFLExtendedJacobians() {
    Jb_LF.block<3,3>(0,3) = Eigen::Matrix3d::Identity();
}

PFLExtendedJacobians::~PFLExtendedJacobians() {
}

void PFLExtendedJacobians::updateJacobians(
		const GeneralizedCoordinates_t & big_q) {
	/*
	 * The Jacobian of the foot contact point (J_fc) is given by,
	 *
	 *  Jb = Jacobian of the base motion
	 *  Jr = Jacobian of the joints motion
	 *
	 *  Jx is expressed w.r.t the BASE frame
	 */

	updateActuatedJacobians(big_q);
	updateFloatingBaseJacobians(big_q);
}

void PFLExtendedJacobians::updateJacobians(
    const JointCoordinates_t & joints_q_r) {

  EEPositionsDataMap_t ee_pose_base;
  GetFeetPoseBaseFromJoints(joints_q_r,ee_pose_base);
  updateAJ_FromJoints(joints_q_r);
  updateFBJ_FromFeetPose(ee_pose_base);
}

void PFLExtendedJacobians::updateJacobiansQuick(
		const JointCoordinates_t & joints_q_r,
		const EEPositionsDataMap_t & feet_pose_base) {

	updateAJ_FromJoints(joints_q_r);
	updateFBJ_FromFeetPose(feet_pose_base);

}

void PFLExtendedJacobians::updateActuatedJacobians(
		const GeneralizedCoordinates_t & big_q) {

	/* Jr_C \in R^(3x3) */

  JointCoordinates_t joints_q_r;
  GetActuatedCoordinates(big_q,joints_q_r);
	updateAJ_FromJoints(joints_q_r);
}

void PFLExtendedJacobians::updateAJ_FromJoints(
		const JointCoordinates_t & joints_q_r) {
	Jr_LF =  legJacobians_.fr_trunk_J_LF_foot(joints_q_r).block<3,3>(iit::rbd::LX,0);
}

void PFLExtendedJacobians::updateFloatingBaseJacobians(
		const GeneralizedCoordinates_t & big_q) {

	/* Jb_C \in R^(3x6) */

  EEPositionsDataMap_t feet_pose_base;
	GetFeetPoseBase(big_q,feet_pose_base);
	updateFBJ_FromFeetPose(feet_pose_base);
}

void PFLExtendedJacobians::updateFBJ_FromFeetPose(
		const EEPositionsDataMap_t & feet_pose_base) {

	for(int leg = 0 ; leg < this->kTotalEE; leg ++){

		GetSkewMatrix(-feet_pose_base[leg],skew_matrix_foot_postion);

		switch(leg) {

			case 0:
				Jb_LF.block<3,3>(0,0) = skew_matrix_foot_postion;
				break;
			default:
				std::cout << "This should never happen" << std::endl;
				std::exit(EXIT_FAILURE);
				break;
		}
	}
}
