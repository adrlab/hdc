#include <dynamical_systems/systems/pointFootLeg/PFLState.hpp>


PFLState::PFLState(const ContactConfiguration_t & fc, const GeneralizedCoordinates_t & q_robot,
                   const GeneralizedCoordinates_t & qdot_robot):PFLConstraintJacobian(fc) {

  initializeGravity();

  //update the robot given position and velocity
  SetNumberOfContacts(getFeetConfiguration());
  update(q_robot,qdot_robot);
}

PFLState::PFLState(const ContactConfiguration_t & fc):PFLConstraintJacobian(fc) {

  initializeGravity();

  //get default configuration and zero velocity
  GeneralizedCoordinates_t q_def,qd_zer;
  GetCanonicalPose(q_def);
  qd_zer.setZero();

  //update the robot
  SetNumberOfContacts(getFeetConfiguration());
  update(q_def,qd_zer);
}

PFLState::PFLState():PFLConstraintJacobian(false) {

  initializeGravity();

  //get default configuration and zero velocity
  GeneralizedCoordinates_t q_def,qd_zer;
  GetCanonicalPose(q_def);
  qd_zer.setZero();

  //update the robot
  SetNumberOfContacts(getFeetConfiguration());
  update(q_def,qd_zer);
}

PFLState::~PFLState() {}

void PFLState::initializeGravity(){
  gravity_world_ << 0 , 0 ,- iit::rbd::g;
}

void PFLState::update(const GeneralizedCoordinates_t & q_robot,
    const GeneralizedCoordinates_t & qd_robot) {

  big_q = q_robot;
  big_qdot = qd_robot;
  StateVectorFromGeneralizedCoordinates(q_robot,qd_robot,x_state);
  updateRobot();
}

void PFLState::updateState(const state_vector_t & x) {
  x_state=x;
  GeneralizedCoordinatesFromStateVector(x_state,big_q,big_qdot);
  updateRobot();
}


void PFLState::changecontactconfiguration(const ContactConfiguration_t &fc) {
  ChangeFeetConfiguration(fc);
  SetNumberOfContacts(fc);
  updateRobot();
}

void PFLState::updateRobot() {

  GetFloatingBaseCoordinates(big_q,base_position_world);
  GetFloatingBaseCoordinates(big_qdot,base_velocity_base);
  GetActuatedCoordinates(big_q,joints_pos);
  GetActuatedCoordinates(big_qdot,joints_vel);

  GetWBTransform(base_position_world,W_X_B);

  W_R_B = W_X_B.linear();
  B_R_W = W_R_B.transpose();
  r_base = W_X_B.translation(); //r_base = wP_b

  GetFeetPoseBaseFromJoints(joints_pos,Feet_Base);
  GetFeetPoseInertia(W_X_B,Feet_Base,Feet_World);

  WorldXbase_motionTransformation(W_X_B,base_velocity_base,base_velocity_world);

  updateQuick(joints_pos,Feet_Base);

  feet_velocity = GetJc()*big_qdot;

  gravity_base_ = B_R_W*gravity_world_;

  SetPositionDerivative();
}

void PFLState::SetNumberOfContacts(const ContactConfiguration_t &fc) {
    number_of_feet_in_contact = 0;
    contact_leg_number.clear();
    for (int i = 0 ; i < this->kTotalEE ; i++) {
      if (fc[i]) {
        number_of_feet_in_contact++;
        contact_leg_number.push_back(i);
      }
    }
}

void PFLState::GetCurrentRealWFeetVelocity(EEPositionsDataMap_t & fv){

  EEPositionsDataMap_t fv_base;
  GetRealFeetVelocity(joints_pos,joints_vel,fv_base);

  fv[0] = W_R_B * fv_base[0];
  fv[1] = W_R_B * fv_base[1];
  fv[2] = W_R_B * fv_base[2];
  fv[3] = W_R_B * fv_base[3];
}

void PFLState::EulerAngleRateOfChange(const Eigen::Vector3d & w , Eigen::Vector3d & dedt) {
  Eigen::Vector3d ypr = base_position_world.segment<3>(0);

  // ZYX-convention From kindr, but interchanging the top and bottom row to have the order roll-pitch-yaw
  Eigen::Matrix3d R;
  R.setZero();
  const double x = ypr(0);
  const double y = ypr(1);
  const double t2 = std::cos(y);
  const double t3 = 1.0/t2;
  const double t4 = std::cos(x);
  const double t5 = std::sin(x);
  const double t6 = std::sin(y);
  R(2,1) = t3*t5;
  R(2,2) = t3*t4;
  R(1,1) = t4;
  R(1,2) = -t5;
  R(0,0) = 1.0;
  R(0,1) = t3*t5*t6;
  R(0,2) = t3*t4*t6;

  dedt = R*w;
}

PFLState& PFLState::operator=(const PFLState& rhs) {
    if(&rhs != this) {
        copydata(rhs);
    }
    return *this;
}

void PFLState::copydata(const PFLState& rhs) {

  changecontactconfiguration(rhs.getFeetConfiguration());
  x_state = rhs.x_state;
  gravity_world_ << 0 , 0 ,- iit::rbd::g;
  updateState(x_state);
}

void PFLState::SetPositionDerivative() {

  Eigen::Vector3d euler_angle_rate;

  EulerAngleRateOfChange(base_velocity_base.segment<3>(0), euler_angle_rate);

  q_derivative.segment<3>(0) = euler_angle_rate;
  q_derivative.segment<3>(3) = W_R_B*base_velocity_base.segment<3>(3);
  q_derivative.segment<this->kTotalJoints>(this->kBaseDof) = joints_vel;
}
