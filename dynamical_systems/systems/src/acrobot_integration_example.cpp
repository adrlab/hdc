/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * acrobot_integration_example.cpp
 *
 *  Created on: Feb 4, 2016
 *      Author: depardo
 */

#include <dynamical_systems/systems/acrobot/acrobotDimensions.hpp>
#include <dynamical_systems/systems/acrobot/acrobot_dynamics.hpp>
#include <dynamical_systems/tools/SystemIntegrationBase.hpp>

int main(int argc , char** argv) {

	typedef typename acrobot::acrobotDimensions::state_vector_t state_vector_t;
	typedef typename acrobot::acrobotDimensions::state_vector_array_t state_vector_array_t;
	typedef typename acrobot::acrobotDimensions::control_vector_array_t control_vector_array_t;

	typedef std::vector<double> time_vector_array_t;

	std::shared_ptr<DynamicsBase<acrobot::acrobotDimensions> > acrobot_dynamics(new acrobotDynamics);

	SystemIntegrationBase<acrobot::acrobotDimensions> syst_int(acrobot_dynamics);

	// Upright position as initial condition
	state_vector_t x_initial;
	x_initial << M_PI/2 , 0 , 0 , 0;

	// integrating 10 seconds at 100Hz
	double frequency = 200;
	double total_time = 300;

	syst_int.Integrate(x_initial,frequency,total_time);

	state_vector_array_t x_solution;
	control_vector_array_t u_solution;
	time_vector_array_t t_solution;

	syst_int.GetSolution(x_solution,u_solution,t_solution);

	std::cout << "x[" << t_solution[0] << "] : " << x_solution[0].transpose() << std::endl;
	std::cout << "u[" << t_solution[0] << "] : " << u_solution[0].transpose() << std::endl;
	std::cout << "x[" << t_solution.back() << "] : " << x_solution.back().transpose() << std::endl;
	std::cout << "u[" << t_solution.back() << "] : " << u_solution.back().transpose() << std::endl;

	std::cout << "The acrobot must be now at rest @t["<<total_time<<"] (seconds), isn't it?"<< std::endl;

	return 0;

}
