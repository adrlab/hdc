/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * acrobot_derivatives_trajectory.cpp
 *
 *  Created on: Jun 9, 2015
 *      Author: depardo
 *
 *
 *      simple example where a trajectory of the derivatives of the system are splined
 */

#include<dynamical_systems/systems/acrobot/acrobotDimensions.hpp>
#include<dynamical_systems/systems/acrobot/acrobot_dynamics.hpp>
#include<dynamical_systems/systems/acrobot/acrobot_derivatives.hpp>

int main(int argc , const char* argv[]) {


	std::shared_ptr<DynamicsBase<acrobot::acrobotDimensions> > dynamics(new acrobotDynamics);

	acrobotNumDiffDerivatives local_derivatives(dynamics);

	acrobot::acrobotDimensions::state_vector_t x;
	acrobot::acrobotDimensions::control_vector_t u;
	acrobot::acrobotDimensions::state_matrix_t A_k;
	acrobot::acrobotDimensions::control_gain_matrix_t B_k;

	x << 0.0 , 0.0 , 0.0, 0.0;
	u << 0.0;

	local_derivatives.setCurrentStateAndControl(x,u);

	A_k = local_derivatives.getDerivativeState();
	B_k = local_derivatives.getDerivativeControl();

	std::cout << "Example of using NumDiffBase class to get the derivatives of a System Dynamics :" << std::endl;
	std::cout << "state : " << x.transpose() << std::endl;
	std::cout << "control : " << u.transpose() << std::endl;
	std::cout << "Derivatives : " << std::endl;

	std::cout << "A_k : " << std::endl << A_k << std::endl;
	std::cout << "B_k : " << std::endl << B_k << std::endl;

	std::cout << "End of example!" << std::endl;
}

