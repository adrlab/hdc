/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*             kTotalStatesControlskTotalStatesControls
 * test_ds_derivatives.cpp
 *
 *  Created on: Nov 11, 2017
 *      Author: depardo
 */
#include <gtest/gtest.h>
#include <Eigen/Dense>
#include <dynamical_systems/systems/acrobot/acrobot_dynamics.hpp>
#include <dynamical_systems/systems/acrobot/acrobot_derivatives.hpp>

namespace {
class test_ds_derivatives : public ::testing::Test {

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

protected:

   test_ds_derivatives() {

     dynamics_= std::shared_ptr<acrobotDynamics>(new acrobotDynamics);
     robot_derivatives_ = std::unique_ptr<acrobotNumDiffDerivatives>(new acrobotNumDiffDerivatives(dynamics_));
   }


   virtual ~test_ds_derivatives() {}

   virtual void SetUp() {
     //changing the seed of the random generator
     std::srand((unsigned int) time(0));
   }

   virtual void TearDown() {
   }

   // Robot Derivatives
   std::unique_ptr<acrobotNumDiffDerivatives> robot_derivatives_;
   std::shared_ptr<DynamicsBase<acrobot::acrobotDimensions> > dynamics_;


};

//// Tests that the Foo::Bar() method does Abc.
TEST_F(test_ds_derivatives, sizes) {

  //Testing: Matrixes size
  {
    EXPECT_EQ(4,robot_derivatives_->A_.cols());
    EXPECT_EQ(4,robot_derivatives_->A_.rows());
    EXPECT_EQ(4,robot_derivatives_->B_.rows());
    EXPECT_EQ(1,robot_derivatives_->B_.cols());
  }

  //Testing: Constants
  {
    EXPECT_EQ(5,robot_derivatives_->kTotalStatesControls);
    EXPECT_EQ(1,robot_derivatives_->kTotalControls);
    EXPECT_EQ(4,robot_derivatives_->kTotalStates);
    EXPECT_EQ(5,robot_derivatives_->inputs.RowsAtCompileTime);
    EXPECT_EQ(1,robot_derivatives_->inputs.ColsAtCompileTime);
    EXPECT_EQ(4,robot_derivatives_->Jacobian.RowsAtCompileTime);
  }

}

TEST_F(test_ds_derivatives,operators) {

  acrobot::acrobotDimensions::state_vector_t out;
  acrobot::acrobotDimensions::control_vector_t ut;
  Eigen::Matrix<double, 5 , 1> in;
  out.setRandom(); in.setRandom();ut.setRandom();

  //Testing: functor operator()
  {
    EXPECT_EQ(5,robot_derivatives_->functor_.InputsAtCompileTime);
    EXPECT_EQ(4,robot_derivatives_->functor_.ValuesAtCompileTime);
    EXPECT_EQ(0,robot_derivatives_->functor_(in,out));
  }

  //Testing: functor shared_ptr<dynamics>
  {
    EXPECT_EQ(4,robot_derivatives_->functor_.local_dynamics->systemDynamics(out,ut).rows());

  }
}

}  // namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
