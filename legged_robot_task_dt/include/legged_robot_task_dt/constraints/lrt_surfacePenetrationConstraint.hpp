/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * lrt_surfacePenetrationConstraint.hpp
 *
 *  Created on: Abril 20, 2016
 *      Author: depardo
 */

#ifndef LRTSURFACEPENETRATIONCONSTRAINT_HPP_
#define LRTSURFACEPENETRATIONCONSTRAINT_HPP_

#include <legged_robot_task_dt/constraints/lrt_singleConstraintBase.hpp>

namespace lrt {

template <class DIMENSIONS>
class LRTSurfacePenetrationConstraint: public LRTSingleConstraintBase<DIMENSIONS> {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
  static const int nee = DIMENSIONS::kTotalEE;

	LRTSurfacePenetrationConstraint(const double & tol, const double & ground_z):
	  LRTSingleConstraintBase<DIMENSIONS>() {

    Eigen::VectorXd lb = Eigen::VectorXd::Constant(nee,ground_z-tol);
    Eigen::VectorXd ub = Eigen::VectorXd::Constant(nee,1e20);

	  this->SetSizeOfConstraint(nee);
	  this->SetBounds(lb,ub);
	}

	void evalLRTConstraint(const state_vector_t & x,
				const control_vector_t & u, Eigen::VectorXd & val);

	~LRTSurfacePenetrationConstraint(){}
};

template <class DIMENSIONS>
void LRTSurfacePenetrationConstraint<DIMENSIONS>::evalLRTConstraint(
		const state_vector_t& x, const control_vector_t& u, Eigen::VectorXd & g_val) {

	g_val.resize(nee);

	for (int i = 0 ; i < nee ; i++ ) {
	    g_val(i) = this->_local_Dynamics->getEEPoseWorld()[i](2);
	}
}

template <class DIMENSIONS>
const int LRTSurfacePenetrationConstraint<DIMENSIONS>::nee;

} // namespace lrt
#endif /* LRTSURFACEPENETRATIONCONSTRAINT_HPP_*/

