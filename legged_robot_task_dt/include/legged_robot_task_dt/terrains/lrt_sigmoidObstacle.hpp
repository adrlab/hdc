/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * lrt_sigmoidObstacle.hpp
 *
 *  Created on: April 24, 2017
 *      Author: depardo
 */

#ifndef _LRT_SIGMOIDOBSTACLE_HPP_
#define _LRT_SIGMOIDOBSTACLE_HPP_

#include <Eigen/Dense>
#include <legged_robot_task_dt/terrains/lrt_terrainMapBase.hpp>

class LRTSigmoidObstacle : public LRTTerrainMapBase {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  LRTSigmoidObstacle(const double & x_obst, const double & y_obst):object_x_position(x_obst),object_y_position(y_obst){}

  double elevationMap(const double & x , const double & y=0) override;
  Eigen::Vector2d elevationMapDerivative(const double & x , const double & y=0) override;

  void setObstacleHeight(const double & h){ obstacle_height = h;}

private:

  double object_x_position = 0;
  double object_y_position = 0;
  double obstacle_height = 0.15;
  double slope = 100;


protected:

};

double LRTSigmoidObstacle::elevationMap(const double &x , const double & y) {

  double xb = -slope*(x - object_x_position);

  double term = 1 + std::exp(xb);

  double z = obstacle_height/term;

  return z;
}

Eigen::Vector2d LRTSigmoidObstacle::elevationMapDerivative(const double &x , const double & y) {

  Eigen::Vector2d grad;
  grad(1) = 0;


  double xb = -slope*(x - object_x_position);

  double c = std::exp(xb);
  double term = 1 + c;
  double z = (obstacle_height * slope*c)/(term*term);

  grad(0) = std::atan(z);

  return grad;
}

#endif /* LRT_SIGMOIDOBSTACLE_HPP_ */
