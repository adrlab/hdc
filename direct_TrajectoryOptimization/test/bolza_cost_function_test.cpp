/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * \dontinclude bolza_cost_function_test.cpp
 */

#ifndef DOXYGEN_SHOULD_SKIP_THIS

#include <iostream>
#include <memory>
#include <Eigen/Dense>
#include <optimization/costs/CostFunctionBase.hpp>
#include <optimization/costs/MinimumTimeCostBolza.hpp>
#include <optimization/costs/MinimumEnergyCostBolza.hpp>
#include <dynamical_systems/systems/acrobot/acrobotDimensions.hpp>

typedef acrobot::acrobotDimensions::state_vector_t state_vector_t;
typedef acrobot::acrobotDimensions::state_vector_array_t state_vector_array_t;
typedef acrobot::acrobotDimensions::control_vector_t control_vector_t;
typedef acrobot::acrobotDimensions::control_vector_array_t control_vector_array_t;

using namespace DirectTrajectoryOptimization;

int main(int argc, const char* argv[]) {

	// minimum time cost
	//MinimumTimeCostBolza<acrobot::acrobotDimensions> my_mt_cfb;
	Costs::MinimumEnergyCostBolza<acrobot::acrobotDimensions> my_me_cfb;

	int trajectory_size = 100;

	state_vector_array_t x_trajectory;
	control_vector_array_t u_trajectory;
	Eigen::VectorXd h_trajectory;

	h_trajectory.resize(trajectory_size-1);
	x_trajectory.resize(trajectory_size);
	u_trajectory.resize(trajectory_size);

	h_trajectory.setConstant(1.0);

	double J2 = 0;

	for (int k = 0 ; k < trajectory_size ; k++) {

		control_vector_t u;

		u.setConstant(2);
		u_trajectory[k] = u;

		state_vector_t x = state_vector_t::LinSpaced(10,100);
		x_trajectory[k] = x;

		double J_k = my_me_cfb.IntermediateCost(x,u,h_trajectory(k));

		J2 = J2 + J_k;
	}

	std::cout << "Test Cost Function (Energy) : " << J2 << std::endl;

	std::shared_ptr<BaseClass::CostFunctionBase<acrobot::acrobotDimensions> > my_base_cost(new Costs::MinimumEnergyCostBolza<acrobot::acrobotDimensions>);

//	my_base_cost->setCurrentTrajectory(x_trajectory,u_trajectory,h_trajectory);
//
//	Eigen::VectorXd test;
//
//	my_base_cost->gradient_xuh(test);
//
//
//
//	std::cout << "gradient xuh : " << test.transpose() << std::endl;

	return 0;
}
#endif
