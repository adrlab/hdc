cmake_minimum_required(VERSION 2.8.3)
project(direct_trajectory_optimization)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  ds_base
  ds_systems
)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wfatal-errors -std=c++11 -Wall")

## System dependencies are found with CMake's conventions
find_package(Eigen3 REQUIRED)

find_library(SNOPT_LIBRARY1 snopt7_cpp   /usr/local/lib/snopt7)
find_library(SNOPT_LIBRARY2 snopt7      /usr/local/lib/snopt7)

find_package(PkgConfig)
pkg_search_module(IPOPT ipopt)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
   INCLUDE_DIRS include
   LIBRARIES
   CATKIN_DEPENDS
#  DEPENDS snopt_cpp snopt7 snprint7 snblas f2c ipopt
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(include)
include_directories(
  ${EIGEN3_INCLUDE_DIR}
  ${catkin_INCLUDE_DIRS}
)

## Executables
# Generic executables
#add_executable(bolza_cost_function_test src/bolza_cost_function_test.cpp)
#add_executable(constraint_base_test src/constraint_base_test.cpp)

#target_link_libraries(bolza_cost_function_test ${catkin_LIBRARIES})
#target_link_libraries(constraint_base_test ${catkin_LIBRARIES})

# SNOPT - only build if SNOPT found
if(SNOPT_LIBRARY1 AND SNOPT_LIBRARY2)

add_executable(acrobot_snopt_example src/acrobot_snopt_example.cpp)
target_compile_definitions( acrobot_snopt_example PRIVATE COMPILE_SNOPT=1)

target_link_libraries(acrobot_snopt_example 
	${SNOPT_LIBRARY1} ${SNOPT_LIBRARY2}  
	${catkin_LIBRARIES}
)

endif()

#IPOPT - only build if IPOPT found
if(IPOPT_FOUND)
include_directories(${IPOPT_INCLUDE_DIRS})
add_executable(acrobot_ipopt_example src/acrobot_ipopt_example.cpp)
target_compile_definitions(acrobot_ipopt_example PRIVATE COMPILE_IPOPT=1)

target_link_libraries(acrobot_ipopt_example
	${IPOPT_LIBRARIES}
	${catkin_LIBRARIES}
)

endif()

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_direct_transcription.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
