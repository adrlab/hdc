/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file MinimumVelocityCostBolza.hpp
 *	\brief Bolza type cost function minimizing the total velocity e = qdTRqd
 *  \author depardo
 */

#ifndef MINIMUMVELOCITYCOSTBOLZA_HPP_
#define MINIMUMVELOCITYCOSTBOLZA_HPP_

#include <Eigen/Dense>
#include <optimization/costs/CostFunctionBolza.hpp>

namespace DirectTrajectoryOptimization {
namespace Costs {

/**
 *  \brief Bolza type cost function minimizing the total energy e = uTRu
 */
template<class DIMENSIONS>
class MinimumVelocityCostBolza : public BaseClass::CostFunctionBolza<DIMENSIONS> {

public :

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::control_matrix_t control_matrix_t;
	typedef typename DIMENSIONS::state_matrix_t state_matrix_t;

	/**
	 * \brief Constructor
	 * \param[in] R The weighting matrix for the energy index
	 */
	MinimumVelocityCostBolza(const state_matrix_t & R = state_matrix_t::Identity()):R_matrix(R)
	{

	}

	virtual ~MinimumVelocityCostBolza() {}

private :

	double phi(const state_vector_t & x, const control_vector_t & u);
	state_vector_t phi_x(const state_vector_t &x, const control_vector_t & u);
	control_vector_t phi_u(const state_vector_t &x, const control_vector_t & u);

	double terminalCost(const state_vector_t &x_f);
	state_vector_t terminalCostDerivative(const state_vector_t &x_f);
	state_matrix_t R_matrix;

};

template <class DIMENSIONS>
double MinimumVelocityCostBolza<DIMENSIONS>::phi(const state_vector_t &x, const control_vector_t & u) {

	double val = (x.transpose()*R_matrix*x)(0);
	return val;
}

template <class DIMENSIONS>
typename DIMENSIONS::state_vector_t MinimumVelocityCostBolza<DIMENSIONS>::phi_x(const state_vector_t &x,
		const control_vector_t & u) {

  state_vector_t val = 2*R_matrix*x;

	return(val);
}

template <class DIMENSIONS>
typename DIMENSIONS::control_vector_t MinimumVelocityCostBolza<DIMENSIONS>::phi_u(const state_vector_t &x,
		const control_vector_t & u) {

	return(control_vector_t::Zero());
}

template <class DIMENSIONS>
double MinimumVelocityCostBolza<DIMENSIONS>::terminalCost(const state_vector_t &x_f) {

	return 0;
}

template <class DIMENSIONS>
typename DIMENSIONS::state_vector_t MinimumVelocityCostBolza<DIMENSIONS>::terminalCostDerivative(const state_vector_t &x_f) {

	return(state_vector_t::Zero());
}

}
}
#endif /* MINIMUMENERGYCOSTBOLZA_HPP_ */
