/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * CylindricalObstaclePathConstraint.hpp
 *
 *  Created on: Nov 18, 2015
 *      Author: depardo
 */
#ifndef DOXYGEN_SHOULD_SKIP_THIS

#ifndef CYLINDRICALOBSTACLEPATHCONSTRAINT_HPP_
#define CYLINDRICALOBSTACLEPATHCONSTRAINT_HPP_

#include <optimization/constraints/ConstraintsBase.hpp>

namespace DirectTrajectoryOptimization {

template <class DIMENSIONS>
class CylindricalObstaclePathConstraint: public BaseClass::ConstraintsBase<DIMENSIONS> {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    typedef typename DIMENSIONS::state_vector_t state_vector_t;
    typedef typename DIMENSIONS::control_vector_t control_vector_t;

	CylindricalObstaclePathConstraint(const double & x_c = 0.0, const double & y_c = 0.0 , const double & r = 0.5):x_center(x_c),y_center(y_c),radius(r){

		initialize();
	}

	void initialize();

	Eigen::VectorXd evalConstraintsFct(const state_vector_t& x, const control_vector_t& u);

	Eigen::VectorXd evalConstraintsFctDerivatives(const state_vector_t & x, const control_vector_t & u);

	~CylindricalObstaclePathConstraint(){}

private:

	double x_center = 0;
	double y_center = 0;
	double radius = 0;

};


template <class DIMENSIONS>
void CylindricalObstaclePathConstraint<DIMENSIONS>::initialize()
{

	this->g_size = 1;

    this->g_low.resize(this->g_size);
    this->g_up.resize(this->g_size);


	double radius_square = radius*radius;

    // set upper and lower bounds (improve this)
    	this->g_low[0] = radius_square;
    	this->g_up[0]  = 1e20;

    int number_of_dependent_variables_at_each_constraint = this->state_size + this->control_size;

	this->nnz_jac_ = number_of_dependent_variables_at_each_constraint
					* this->g_size;

    this->iRow_.resize(this->nnz_jac_);
    this->jCol_.resize(this->nnz_jac_);

	Eigen::VectorXi temp;
	temp = Eigen::VectorXi::LinSpaced(this->g_size, 0, this->g_size - 1);
	this->iRow_ = temp.replicate(
				  number_of_dependent_variables_at_each_constraint, 1);

	temp = 	Eigen::VectorXi::LinSpaced(
			number_of_dependent_variables_at_each_constraint, 0,
			number_of_dependent_variables_at_each_constraint - 1);
	this->jCol_ = temp.replicate(this->g_size, 1);
}


template <class DIMENSIONS>
Eigen::VectorXd CylindricalObstaclePathConstraint<DIMENSIONS>::evalConstraintsFct(const state_vector_t& x, const control_vector_t& u)
{

	Eigen::VectorXd g_vector(this->g_size);


	g_vector[0] = std::pow(x(0)- x_center,2) + std::pow(x(1)-y_center,2);

	//g_vector[0] = x(0);

	return g_vector;

}

template <class DIMENSIONS>
Eigen::VectorXd CylindricalObstaclePathConstraint<DIMENSIONS>::evalConstraintsFctDerivatives(const state_vector_t & x , const control_vector_t &u)
{


	// This is not the right way of providing the derivatives as here we are assuming certain system-DIMENSIONS,
	// either we change that for "CylindricalObstacleQuadrotor" or
	// add a numdiff structure to get the gradient or create a QuadrotorConstraintBase class

	Eigen::VectorXd Jacobian(this->nnz_jac_);

 // first constraint
	// first w.r.t states
	Jacobian[0] = 2*(x(0) - x_center);
	Jacobian[1] = 2*(x(1) - y_center);
	Jacobian[2] = 0;
	Jacobian[3] = 0;
	Jacobian[4] = 0;
	Jacobian[5] = 0;
	Jacobian[6] = 0;
	Jacobian[7] = 0;
	Jacobian[8] = 0;
	Jacobian[9] = 0;
	Jacobian[10] = 0;
	Jacobian[11] = 0;

	// now w.r.t. control
	Jacobian[12] = 0;
	Jacobian[13] = 0;
	Jacobian[14] = 0;
	Jacobian[15] = 0;


	return Jacobian;

}
}
#endif /* CYLINDRICALOBSTACLEPATHCONSTRAINT_HPP_ */
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

