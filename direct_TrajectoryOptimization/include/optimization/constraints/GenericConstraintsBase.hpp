/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file 	GenericConstraintsBase.hpp
 *	\brief	Base class for vector constraints and numdiff derivatives
 * 	\author	depardo
 */

#ifndef _GENERICCONSTRAINTSBASE_H
#define _GENERICCONSTRAINTSBASE_H

#include <Eigen/Dense>
#include <memory>
#include <optimization/NumDiffDerivativesBase.hpp>

namespace DirectTrajectoryOptimization {
namespace BaseClass{

/**
 * @brief Base Class to define vector of constraints and its derivatives
 */
class GenericConstraintsBase : public NumDiffDerivativesBase , public std::enable_shared_from_this<GenericConstraintsBase> {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	/**
	 * @brief Generic Constraint Constructor
	 * @param[in] num_eq The size of the vector of constraints
	 * @param[in] num_var The number of variables
	 */
	GenericConstraintsBase(int num_eq , int num_var):g_size_(num_eq),input_size_(num_var){};
    

	/**
	 * @brief To be overloaded by the derived class
	 * @details Constraint function to be evaluated
	 */
    virtual Eigen::VectorXd Evalfx(const Eigen::VectorXd & input) = 0;

    virtual void fx(const Eigen::VectorXd & in , Eigen::VectorXd & out) override;

	/**
	 * @brief To be overloaded by the derived class in case NumDiff is not required
	 * @return The Jacobian matrix of the constraint
	 */
    virtual Eigen::MatrixXd EvalFxJacobian(const Eigen::VectorXd & input);

    /**
     * @brief Method to get the lower bound of the constraint
     * @returns The vector of lower bounds of the constraint set
     */
    Eigen::VectorXd GetConstraintsLowerBound() {return g_low_;}

    /**
     * @brief Method to get the upper bound of the constraint
     * @returns The vector of upper bounds of the constraint set
     */
    Eigen::VectorXd GetConstraintsUpperBound() {return g_up_;}

    /**
     * @brief Method to set the lower bound of the constraint
     * @param[in] g_lb_candidate Eigen vector of dynamic size
     */
    void SetConstraintsLowerBound(const Eigen::VectorXd & g_lb_candidate);

    /**
     * @brief Method to set the upper bound of the constraint
     * @param[in] g_ub_candidate Eigen vector of dynamic size
     */
    void setConstraintsUpperBound(const Eigen::VectorXd & g_ub_candidate);

    int GetNumConstraints() {return g_size_;}
    
    int GetNnzJac() {return nnz_jac_;}

    Eigen::VectorXi GetSparseRow() {return iRow_;}
    Eigen::VectorXi GetSparseCol() {return jCol_;}
    
    void initialize_num_diff();

    virtual ~GenericConstraintsBase() {};


protected:

    std::shared_ptr<GenericConstraintsBase> self_pointer;

    int g_size_ = 0;
    int input_size_  = 0;
    int nnz_jac_=0;

    Eigen::VectorXd g_low_;
    Eigen::VectorXd g_up_;

    Eigen::VectorXi iRow_;
    Eigen::VectorXi jCol_;

};

void GenericConstraintsBase::fx(const Eigen::VectorXd & input, Eigen::VectorXd & output){

	output = this->Evalfx(input);

}

Eigen::MatrixXd GenericConstraintsBase::EvalFxJacobian(const Eigen::VectorXd & input){

	// NumDiff Base Jacobian of the constraint. User can override this method in the
	// derived constraint. But this class provides a default Jacobian

	mJacobian.setZero();
	this->numDiff->df(input,this->mJacobian);

	return(mJacobian);

}

void GenericConstraintsBase::SetConstraintsLowerBound(const Eigen::VectorXd & g_lb_candidate){
	this->g_low_ = g_lb_candidate;
}

void GenericConstraintsBase::setConstraintsUpperBound(const Eigen::VectorXd & g_ub_candidate){
	this->g_up_ = g_ub_candidate;
}

void GenericConstraintsBase::initialize_num_diff(){

	mJacobian.resize(g_size_,input_size_);

	this->self_pointer = std::enable_shared_from_this<GenericConstraintsBase>::shared_from_this();

	this->numdifoperator = std::shared_ptr<FunctionOperator>(new FunctionOperator(input_size_, g_size_ , this->self_pointer));

	this->numDiff = std::shared_ptr<Eigen::NumericalDiff<FunctionOperator> >( new Eigen::NumericalDiff<FunctionOperator>(*this->numdifoperator , std::numeric_limits<double>::epsilon()));

}
}
}
#endif /* _GENERICCONSTRAINTSBASE_H */
