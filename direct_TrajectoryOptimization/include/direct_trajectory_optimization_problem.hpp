/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file 		direct_trajectory_optimization_problem.hpp
 *	\brief		Main class of the DTO package
 *  \authors  singhh23, depardo, rgrandia
 */

/*! \mainpage Direct Trajectory Optimization Package
 * 	\brief A C++ package for solving Trajectory Optimization problems using direct transcription and multiple shooting methods
 *
 * \section intro_sec Overview
 *
 * This package provides a class to formulate a multi-phase continuous trajectory optimization problem including initial and terminal conditions,
 * path constraints and bounds in the state and control trajectories.
 *
 * This class automatically translate the continuous formulation into a discrete optimization problem and uses a (third-party) NLP solver to obtain
 * a feasible and optimal solution.
 *
 * Main features :
 *
 * - Direct transcription using Hermite-Simpson and Trapezoidal interpolation
 * - Direct Multiple Shooting
 * - Interface with <a href="https://projects.coin-or.org/Ipopt">Ipopt</a> and <a href="http://www.sbsi-sol-optimize.com/asp/sol_product_snopt.htm">Snopt</a>.
 * - Multiple phase TO problem can be defined (i.e., including switching dynamics)
 *
 * \subsection your_problem Formulating your TO problem
 *
 * The package includes a set of base classes so you can easily interface this library with any C++ implementation of the dynamics (and derivatives)
 * of your robot. It also includes base classes that automatically provide the derivatives of the robot dynamics and problem constraints using
 * numerical differentiation (Eigen::NumDiff).
 *
 * The main class of the package is called \link DirectTrajectoryOptimization::DirectTrajectoryOptimizationProblem DirectTrajectoryOptimizationProblem \endlink
 * and it is a good point to start exploring this documentation.
 *
 * You also might want to check the objects interfacing the NLP Solvers,
 * - DirectTrajectoryOptimization::DTOSnoptInterface
 * - DirectTrajectoryOptimization::DTOIpoptInterface
 *
 * Example applications using this package are available in the <a href="examples.html"> examples</a> section.
 *
 * \subsection credits Credits
 *
 * This project is led by <a href="http://www.diegopardo.net">Diego Pardo</a> (core developer) and contains knowledge and expertise
 * from the <a href="http://www.adrl.ethz.ch"> Agile and Dexterous Robotics Lab</a> at ETH Zürich, under the supervision of
 * <a href="http://www.adrl.ethz.ch/doku.php/adrl:people:jbuchli">Prof. Jonas Buchli</a>.
 *
 * Collaborators:
 * - Michael Neunert
 * - Harsoveet Singh
 * - Lukas Möller
 * - Alexander Winkler
 * - Bob Hoffmann
 *
 * \subsection license License
 *
 * This software is subject to the terms and conditions defined in
 * file <a href="License.txt">LICENSE.txt</a>, which is part of this source code package.
 */
// system
#include <Eigen/Core>
#include <unistd.h>
#include <vector>
#include <memory>
// dynamical_systems
#include <dynamical_systems/base/DynamicsBase.hpp>
#include <dynamical_systems/base/DerivativesBaseDS.hpp>
#include <method_interfaces/base_method_interface.hpp>
#include <method_interfaces/direct_transcription/direct_transcription.hpp>
#include <method_interfaces/multiple_shooting/multiple_shooting.hpp>
#include <solver_interfaces/base_solver_interface.hpp>

#ifdef COMPILE_IPOPT
#include <solver_interfaces/ipopt/ipopt.hpp>
#endif

#ifdef COMPILE_SNOPT
#include <solver_interfaces/snopt/snopt.hpp>
#endif

#ifndef INCLUDE_DIRECT_TRAJECTORY_OPTIMIZATION_PROBLEM_HPP_
#define INCLUDE_DIRECT_TRAJECTORY_OPTIMIZATION_PROBLEM_HPP_

/**
 * @brief Main namespace
 */
namespace DirectTrajectoryOptimization {

/**
 * @brief An enumeration of the direct trajectory optimization methods that are available
 * for use with the DirectTrajectoryOptimizationProblem class.
 */
namespace Methods {
/*! Direct Methods for TO */
typedef enum Method {
	DIRECT_TRANSCRIPTION = 0,	/*!< Direct Transcription */
	MULTIPLE_SHOOTING			/*!< Multiple Shooting */
} method_t;
}

/**
 * @brief An enumeration of the solvers that are available for use with the DirectTrajectoryOptimizationProblem class.
 */
namespace Solvers {
/*! NLP Solvers */
typedef enum Solver {
	SNOPT_SOLVER = 0,	/*!< SNOPT */
	IPOPT_SOLVER		/*!< IPOPT */
} solver_t;
}

/**
 * @brief This class allows users to solve direct trajectory optimization problems.
 * using different solvers (ipopt/snopt) and different direct methods for trajectory optimization.
 * @tparam DIMENSIONS the class defining the vector types and structures with adequate sizes.
 */
template <typename DIMENSIONS>
class DirectTrajectoryOptimizationProblem {

public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
	typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;

	/**
	 * @brief Create a new DTOSolver instance.
	 * @param[in] dynamics Vector of the dynamics of your problem.
	 * @param[in] derivatives Vector of derivatives of your problem.
	 * @param[in] costFunction The chosen cost function for the NLP.
	 * @param[in] constraints The chosen vector of constraints for the NLP.
	 * @param[in] duration Time bounds (Tmin, Tmax).
	 * @param[in] numberOfNodes Number of nodes for trajectory discretization.
	 * @param[in] solverType The solver of to employ for the solution of this NLP - see related enum for options.
	 * @param[in] methodType The particular direct method to employ for trajectory optimization - see related enum for options.
	 * @param[in] solverVerbose Show output of solver iterations.
	 * @param[in] methodVerbose Show output of discretization process.
	 * @param[in] feasiblePointOnly The type of optimization.
	 * @return A unique pointer to the created solver.
	 */
	static std::unique_ptr<DirectTrajectoryOptimizationProblem<DIMENSIONS> > CreateWithMultipleDynamics(
			std::vector<std::shared_ptr<DynamicsBase<DIMENSIONS> > > dynamics,
			std::vector<std::shared_ptr<DerivativesBaseDS<DIMENSIONS> > > derivatives,
			std::shared_ptr<BaseClass::CostFunctionBase<DIMENSIONS> > costFunction,
			std::vector<std::shared_ptr<BaseClass::ConstraintsBase<DIMENSIONS> > > constraints,
			Eigen::Vector2d duration, int numberOfNodes,
			Solvers::solver_t solverType, Methods::method_t methodType,
			bool solverVerbose, bool methodVerbose, bool feasiblePointOnly=false) {

		// generate the class instance
		std::unique_ptr<DirectTrajectoryOptimizationProblem<DIMENSIONS> > problem( new DirectTrajectoryOptimizationProblem<DIMENSIONS>() );

		// generate the method interface
		switch(methodType) {

		case Methods::DIRECT_TRANSCRIPTION:
			problem->_method = std::shared_ptr<DTOMethodInterface<DIMENSIONS> >( new DTODirectTranscription<DIMENSIONS>(
									dynamics, derivatives, costFunction, constraints,
									duration, numberOfNodes, methodVerbose));	// ToDo add back dircol selection
			break;

		case Methods::MULTIPLE_SHOOTING:

			problem->_method = std::shared_ptr<DTOMethodInterface<DIMENSIONS> >( new DTOMultipleShooting<DIMENSIONS>(
									dynamics, derivatives, costFunction, constraints,
									duration, numberOfNodes, methodVerbose));
			break;

		default:
			std::cout << "METHOD NOT Implemented" << std::endl;
			exit(EXIT_FAILURE);
		}

		// generate the solver interface
		switch(solverType) {

#ifdef COMPILE_SNOPT
		case Solvers::SNOPT_SOLVER:
			problem->_solver = std::shared_ptr<DTOSolverInterface<DIMENSIONS> > (new DTOSnoptInterface<DIMENSIONS>(
					problem->_method, solverVerbose,feasiblePointOnly));
			break;
#endif

#ifdef COMPILE_IPOPT
		case Solvers::IPOPT_SOLVER:
			problem->_solver = std::shared_ptr<DTOSolverInterface<DIMENSIONS> > (new DTOIpoptInterface<DIMENSIONS>(
					problem->_method, solverVerbose));
			break;
#endif

		default:
			std::cout << "SOLVER NOT Available" << std::endl;
			exit(EXIT_FAILURE);
		}

		return problem;
	}

	/**
	 * @brief Create a new DTOSolver instance for single phase problem.
	 * @param[in] dynamics The dynamics of your problem.
	 * @param[in] derivatives The derivatives of your problem.
	 * @param[in] costFunction The chosen cost function for the NLP.
	 * @param[in] constraints The chosen vector of constraints for the NLP.
	 * @param[in] duration Time bounds (Tmin, Tmax)
	 * @param[in] numberOfNodes Number of nodes for trajectory discretization.
	 * @param[in] solverType The solver of to employ for the solution of this NLP - see related enum for options.
	 * @param[in] methodType The particular direct method to employ for trajectory optimization - see related enum for options.
	 * @param[in] solverVerbose Show output of solver iterations.
	 * @param[in] methodVerbose Show output of discretization process.
	 * @param[in] feasiblePointOnly The type of optimization.
	 * @return A unique pointer to the created solver.
	 */
    static std::unique_ptr<DirectTrajectoryOptimizationProblem<DIMENSIONS> > Create(
    		std::shared_ptr<DynamicsBase<DIMENSIONS> > dynamics,
			std::shared_ptr<DerivativesBaseDS<DIMENSIONS> > derivatives,
			std::shared_ptr<BaseClass::CostFunctionBase<DIMENSIONS> > costFunction,
			std::shared_ptr<BaseClass::ConstraintsBase<DIMENSIONS> > constraints,
			Eigen::Vector2d duration, int numberOfNodes,
			Solvers::solver_t solverType, Methods::method_t methodType,
			bool solverVerbose, bool methodVerbose, bool feasiblePointOnly=false) {
    	std::vector<std::shared_ptr<DynamicsBase<DIMENSIONS> > > dynamicsVector;
    	std::vector<std::shared_ptr<DerivativesBaseDS<DIMENSIONS> > > derivativesVector;
    	std::vector<std::shared_ptr<BaseClass::ConstraintsBase<DIMENSIONS> > > constraintsVector;

		dynamicsVector.push_back(dynamics);
		derivativesVector.push_back(derivatives);
		constraintsVector.push_back(constraints);

		return CreateWithMultipleDynamics(dynamicsVector, derivativesVector, costFunction, constraintsVector, duration,
				numberOfNodes, solverType, methodType, solverVerbose, methodVerbose,feasiblePointOnly);
    }

	/**
	 * @brief Destruct the given DTOSolver instance.
	 */
	~DirectTrajectoryOptimizationProblem() {};

	/**
	 * @brief Initialize the DTOSolver problem.
	 * @param[in] problemName The name of the problem.
	 * @param[in] outputFile The name of the output file.
	 * @param[in] paramFile The parameters file (non implemented)
	 * @param[in] computeJacobian Flag indicating user is providing Jacobians
	 */
	void InitializeSolver(
			const std::string & problemName,
			const std::string & outputFile,
			const std::string & paramFile /*= "" */,
			const bool &computeJacobian) {
		_solver->InitializeSolver(problemName, outputFile, paramFile, computeJacobian);
	}

	void InitializeNLPvector(Eigen::VectorXd & x,
			const state_vector_array_t & state_initial,
			const control_vector_array_t & control_initial,
			const Eigen::VectorXd & h_initial) {

		_method->InitializeNLPvector(x, state_initial, control_initial, h_initial);
	}


	/**
	 * @brief Set initial state as hard constraint
	 */
	void SetInitialState(const state_vector_t & state){
		_method->SetInitialState(state);
	}

	/**
	 * @brief Set final state as hard constraint
	 */
	void SetFinalState(const state_vector_t & state) {
		_method->SetFinalState(state);
	}

	/**
	 * @brief Set final velocity zero as hard constraint
	 */
	void SetFinalVelocityZero(const Eigen::VectorXi & vel_indexes)
	{
		_method->SetFinalVelocityZero(vel_indexes);
	}

	/**
	 * @brief Set initial control
	 */
	void SetInitialControl(const control_vector_t & control) {
		_method->SetInitialControl(control);
	}

	/**
	 * @brief Set final control
	 */
	void SetFinalControl(const control_vector_t & control) {
		_method->SetFinalControl(control);
	}

	/**
	 * @brief Set even time increments as hard constraint
	 */
	void SetEvenTimeIncrements(const bool & opt) {
		_method->SetEvenTimeIncrements(opt);
	}

	/**
	 * @brief Set percentages of nodes per phase
	 * @param[in] nodeSplit Eigen integer vector indicating percentage of nodes for each phase
	 */
	void SetPercentageNodesPerDynamics(Eigen::VectorXd nodeSplit) {
		_method->SetPercentageNodesPerDynamics(nodeSplit);
	}

	/**
	 * @brief Set pointer to function evaluating guard function
	 * @param[in] fcnVector Pointer to the guard function
	 */
	void SetGuardFunction(std::vector<double (*)(state_vector_t&)> fcnVector) {
		_method->SetGuardFunction(fcnVector);
	}

	/**
	 * @brief Solves the DTO problem
	 * @param[in] warminit Flag indicating warmstart
	 */
	void Solve(bool warminit = false) {
		_solver->Solve(warminit);
	}

	/**
	 * @brief Change Verbosity level for solver and method
	 * @param[in] verbose Flag indicating verbosity
	 */
	void SetVerbosity(bool verbose) {
		_solver->SetVerbosity(verbose);
		_method->SetVerbosity(verbose);
	}

	/**
	 * @brief Turn on/off iteration output to the screen
	 * @param[in] p_iter a bool for turning on and off iteration printout
	 */
	void ShowIterations(bool p_iter){
	  _solver->setPrintIterations(p_iter);
	}

	/**
	 * @brief Get state control and time solution
	 * @param[out] yTraj The state trajectory solution
	 * @param[out] uTraj The control trajectory solution
	 * @param[out] hTraj the time increments solution
	 */
	void GetDTOSolution(
			state_vector_array_t &yTraj,
			control_vector_array_t &uTraj,
			Eigen::VectorXd &hTraj) {
		_solver->GetDTOSolution(yTraj, uTraj, hTraj);
	}

	/**
	 * @brief Get state control , time and phaseId solution
	 * @param[out] yTraj The state trajectory solution
	 * @param[out] uTraj The control trajectory solution
	 * @param[out] hTraj the time increments solution
	 * @param[out] phaseId The phase Id solution
	 */
	void GetDTOSolution(
			state_vector_array_t &yTraj,
			control_vector_array_t &uTraj,
			Eigen::VectorXd &hTraj,
			std::vector<int> &phaseId){
		_solver->GetDTOSolution(yTraj, uTraj, hTraj, phaseId);
	}

	void GetDTOSolution(
	    state_vector_array_t &yTraj,
	    state_vector_array_t &ydotTraj,
	    control_vector_array_t &uTraj,
	    Eigen::VectorXd & hTraj,
	    std::vector<int> &phaseId) {

	  _solver->GetDTOSolution(yTraj, ydotTraj, uTraj, hTraj,phaseId);

	}

	/**
	 * @brief Get final value of constraint vector
	 * @param[out] fVec The vector of cost function and constraints
	 */
	void GetFVector(Eigen::VectorXd &fVec) {
		_solver->GetFVector(fVec);
	}

	/**
	 * @brief Change the number of points after construction
	 */
	int ChangeNumberOfPoints() {
		return(_method->ChangeNumberOfpoints());
	}

	/**
	 * @brief Set constant state trajectory bounds
	 * @param[in] lb state lower bound
	 * @param[in] ub state upper bound
	 */
	void SetStateBounds(const state_vector_t & lb, const state_vector_t & ub) {
		_method->SetStateBounds(lb,ub);
	}

	/**
	 * @brief Set trajectory of state bounds
	 * @param[in] lb state trajectory of lower bounds
	 * @param[in] ub state trajectory of upper bounds
	 */
	void SetStateBounds(const state_vector_array_t & lb, const state_vector_array_t & ub){
		_method->SetStateBounds(lb,ub);
	}

	/**
	 * @brief Set constant control trajectory bounds
	 * @param[in] lb control lower bound
	 * @param[in] ub control upper bound
	 */
	void SetControlBounds(const control_vector_t & lb, const control_vector_t & ub) {
		_method->SetControlBounds(lb,ub);
	}

	/**
	 * @brief Set trajectory of control bounds
	 * @param[in] lb control trajectory of lower bounds
	 * @param[in] ub control trajectory of upper bounds
	 */
	void SetControlBounds(const control_vector_array_t & lb, const control_vector_array_t & ub) {
		_method->SetControlBounds(lb,ub);
	}

	/**
	 * @brief Set final state free of bounds
	 * @param[in] state_number The index of the state to be free
	 */
	void SetFreeFinalState(int state_number) {
		_method->SetFreeFinalState(state_number);
	}

	/**
	 * @brief Set bounds for the time increments between nodes
	 * @param[in] h_min positive minimum time increment
	 * @param[in] h_max positive maximum time increment
	 */
	void SetTimeIncrementsBounds(const double & h_min, const double & h_max) {
		_method->setTimeIncrementsBounds(h_min,h_max);
	}

	/**
	 * @brief SetDircolMethod
	 * @param[in] dmc 1 The type of direct collocation method (1-Hermite-Simpson 0-Trapezoidal)
	 */
	void SetDircolMethod(const int & dcm)
	{
		_method->SetDircolMethod(dcm);
	}

	std::shared_ptr<DTOMethodInterface<DIMENSIONS> > _method; /*!< The pointer to the DT method */
	std::shared_ptr<DTOSolverInterface<DIMENSIONS> > _solver; /*!< The pointer to the Solver */


private:

	/**
	 * @brief Default private constructor
	 */
	DirectTrajectoryOptimizationProblem() {};
};

} // namespace DirectTrajectoryOptimization
#endif /* INCLUDE_DIRECT_TRAJECTORY_OPTIMIZATION_PROBLEM_HPP_ */
