/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file 	direct_transcription_implementation.hpp
 *	\author depardo , moellelu, rgrandia
 */

#include <Eigen/Dense>
#include <cstdlib>
#include <iostream>

#include "direct_transcription.hpp"

namespace DirectTrajectoryOptimization {

template <class DIMENSIONS>
void DTODirectTranscription<DIMENSIONS>::SetDircolMethod(const int & dcm) {

	dircol_method = dcm;
}

template <class DIMENSIONS>
void DTODirectTranscription<DIMENSIONS>::evaluateDefects(
		Eigen::VectorXd & defect_vector) {

  defect_vector.setZero();

	// ToDo : Move this to the update variables method and validate with : dircol==1
	state_vector_t y_kmid;
	state_vector_t F_kmid, ymid_dot;
	control_vector_t u_kmid;

	int node = 0;
	int current_defect_index = 0;
	int phase_first_node = 0;
	int inc_index=0;

	for(int p = 0; p < this->num_phases ; p++ ){

		for(int n = 0 ; n < this->number_nodes_phase[p] - 1 ; n++) {

			node = phase_first_node + n;

			// Trapezodial
			if (dircol_method == 0)	{

				defect_vector.segment(current_defect_index, this->_numStates) = -this->_y_trajectory[node+1] + this->_y_trajectory[node] +
						this->_h_trajectory(inc_index)*0.5*( this->_ydot_trajectory[node] + this->_ydot_trajectory[node+1] );

			} else if(dircol_method == 1) {

				// Hermitian interpolation of state at intermediate points
				y_kmid = 0.5*(this->_y_trajectory[node] + this->_y_trajectory[node+1]) + this->_h_trajectory(inc_index)*0.125*(this->_ydot_trajectory[node] - this->_ydot_trajectory[node+1]);
				// Midpoint rule for control at intermediate points
				u_kmid = 0.5*(this->_u_trajectory[node] + this->_u_trajectory[node+1]);

				// approximated dynamics at intermediate point assuming
				ymid_dot = (1.5/this->_h_trajectory(inc_index))*(this->_y_trajectory[node+1] - this->_y_trajectory[node]) - 0.25*(this->_ydot_trajectory[node] + this->_ydot_trajectory[node+1]);

				// dynamics at the intermediate points
				F_kmid = this->_dynamics[this->node_to_phase_map[node]]->systemDynamics(y_kmid, u_kmid);

				//finally the defect constraints
				defect_vector.segment(current_defect_index, this->_numStates) = F_kmid - ymid_dot;
			}
			current_defect_index += this->_numStates;
			inc_index++;

		}
		phase_first_node += this->number_nodes_phase[p];


	}
}

template <class DIMENSIONS>
void DTODirectTranscription<DIMENSIONS>::evalSparseJacobianConstraint(double* val) {

    Eigen::Map<Eigen::VectorXd> val_vec(val,this->myNLP.lenG);

    int constraint_row = 0; // row of the constraint
    int G_index = 0;      	// current index of the sparsity structure
    int nnz_per_row;
    
    // defect constraints
    	// Trapezodial
		if (dircol_method == 0) {
			//state_vector_t F_k, F_kp1;
			state_vector_t Jac_h;
			state_matrix_t Jac_yk, Jac_ykp1;
			control_gain_matrix_t Jac_uk, Jac_ukp1;

			double alpha = 0;
			int node = 0;
			int phase_first_node = 0;
			int inc_index = 0;

			for(int p = 0; p < this->num_phases ; p++ ){

				// initialize iteration variables
				this->_derivatives[this->node_to_phase_map[phase_first_node]]->setCurrentStateAndControl(this->_y_trajectory[phase_first_node],this->_u_trajectory[phase_first_node]);
				state_matrix_t aux_s_matrix = this->_derivatives[this->node_to_phase_map[phase_first_node]]->getDerivativeState();
				control_gain_matrix_t aux_c_matrix = this->_derivatives[this->node_to_phase_map[phase_first_node]]->getDerivativeControl();


				for(int n = 0 ; n < this->number_nodes_phase[p] - 1 ; n++) {

					node = phase_first_node + n;

					alpha = 0.5*this->_h_trajectory(inc_index);

					//F_kp1 = this->_dynamics[this->node_to_phase_map[k+1]]->systemDynamics(y_trajectory[k+1], u_trajectory[k+1]);

					//w.r.t h (room for efficiency)
					Jac_h = 0.5*(this->_ydot_trajectory[node]+ this->_ydot_trajectory[node+1]);
					//w.r.t y_k
					Jac_yk = I_y + alpha * aux_s_matrix;
					//w.r.t u_k
					Jac_uk = alpha * aux_c_matrix;

					//Very important : Do not move this above previous Jac's.
					this->_derivatives[this->node_to_phase_map[node+1]]->setCurrentStateAndControl(this->_y_trajectory[node+1],this->_u_trajectory[node+1]);
					aux_s_matrix = this->_derivatives[this->node_to_phase_map[node+1]]->getDerivativeState();
					aux_c_matrix = this->_derivatives[this->node_to_phase_map[node+1]]->getDerivativeControl();

					//w.r.t y_kp1
					Jac_ykp1 = -I_y + alpha * aux_s_matrix;
					//w.r.t u_kp1
					Jac_ukp1 = alpha * aux_c_matrix;

					for(int jj=0; jj<this->_numStates; jj++) {

						val_vec(G_index) = Jac_h(jj);
						G_index += 1;
						val_vec.segment(G_index, this->_numStates) = Jac_yk.row(jj);
						G_index += this->_numStates;
						val_vec.segment(G_index, this->_numStates) = Jac_ykp1.row(jj);
						G_index += this->_numStates;
						val_vec.segment(G_index, this->_numControls) = Jac_uk.row(jj);
						G_index += this->_numControls;
						val_vec.segment(G_index, this->_numControls) = Jac_ukp1.row(jj);
						G_index += this->_numControls;

						constraint_row++;
					}
					inc_index++;
				}
				phase_first_node += this->number_nodes_phase[p];
			}

		} else if (dircol_method == 1) { // Simpson-Hermite

			state_vector_t y_kmid, F_kmid;
			control_vector_t u_kmid;

			state_vector_t Jac_h;
			state_matrix_t Jac_yk, Jac_ykmid, Jac_ykp1;
			state_matrix_t dFk_dyk, dFkmid_dykmid, dFkp1_dykp1;
			control_gain_matrix_t Jac_uk, Jac_ukmid, Jac_ukp1;
			control_gain_matrix_t dFk_duk, dFkmid_dukmid, dFkp1_dukp1;

			double fac = 0.0;

			int node = 0;
			int phase_first_node = 0;
			int inc_index = 0;

			for(int p = 0; p < this->num_phases ; p++ ){

				for(int n = 0 ; n < this->number_nodes_phase[p] - 1 ; n++) {

					node = phase_first_node + n;

					fac = 3/(2*this->_h_trajectory(inc_index));

					//F_k = this->_dynamics[this->node_to_phase_map[k]]->systemDynamics(y_trajectory[k], u_trajectory[k]);
					//F_kp1 = this->_dynamics[this->node_to_phase_map[node+1]]->systemDynamics(y_trajectory[node+1], u_trajectory[node+1]);

					y_kmid = 0.5*(this->_y_trajectory[node] + this->_y_trajectory[node+1]) + this->_h_trajectory(inc_index)*0.125*(this->_ydot_trajectory[node] - this->_ydot_trajectory[node+1]);

					u_kmid = 0.5*(this->_u_trajectory[node] + this->_u_trajectory[node+1]);

					F_kmid = this->_dynamics[this->node_to_phase_map[node]]->systemDynamics(y_kmid, u_kmid);

					// Dynamic derivatives
					this->_derivatives[this->node_to_phase_map[node]]->setCurrentStateAndControl(this->_y_trajectory[node],this->_u_trajectory[node]);
					dFk_dyk = this->_derivatives[this->node_to_phase_map[node]]->getDerivativeState();
					dFk_duk = this->_derivatives[this->node_to_phase_map[node]]->getDerivativeControl();

					this->_derivatives[this->node_to_phase_map[node]]->setCurrentStateAndControl(y_kmid,u_kmid);
					dFkmid_dykmid = this->_derivatives[this->node_to_phase_map[node]]->getDerivativeState();
					dFkmid_dukmid = this->_derivatives[this->node_to_phase_map[node]]->getDerivativeControl();

					this->_derivatives[this->node_to_phase_map[node+1]]->setCurrentStateAndControl(this->_y_trajectory[node+1],this->_u_trajectory[node+1]);
					dFkp1_dykp1 = this->_derivatives[this->node_to_phase_map[node+1]]->getDerivativeState();
					dFkp1_dukp1 = this->_derivatives[this->node_to_phase_map[node+1]]->getDerivativeControl();

					//Elements of Jacobian
						//w.r.t h
					Jac_h = 0.125 * dFkmid_dykmid * (this->_ydot_trajectory[node] - this->_ydot_trajectory[node+1])
							- (fac/this->_h_trajectory(inc_index))*(this->_y_trajectory[node] - this->_y_trajectory[node+1]); //
						//w.r.t y_k
					Jac_yk = this->_h_trajectory(inc_index)*0.125*dFkmid_dykmid*dFk_dyk + 0.5*dFkmid_dykmid + I_y*fac + 0.25*dFk_dyk; //
						//w.r.t u_k
					Jac_uk = this->_h_trajectory(inc_index)*0.125*dFkmid_dykmid*dFk_duk + 0.5*dFkmid_dukmid + 0.25*dFk_duk; //
						//w.r.t y_kp1
					Jac_ykp1 = -this->_h_trajectory(inc_index)*0.125*dFkmid_dykmid*dFkp1_dykp1 + 0.5*dFkmid_dykmid - fac*I_y + 0.25*dFkp1_dykp1; //
						//w.r.t u_kp1
					Jac_ukp1 = -this->_h_trajectory(inc_index)*0.125*dFkmid_dykmid*dFkp1_dukp1 + 0.5*dFkmid_dukmid + 0.25*dFkp1_dukp1; //


					for (int jj = 0 ; jj < this->_numStates; jj++) {
						val_vec(G_index) = Jac_h(jj);
						G_index += 1;
						val_vec.segment(G_index, this->_numStates) = Jac_yk.row(jj);
						G_index += this->_numStates;
						val_vec.segment(G_index, this->_numStates) = Jac_ykp1.row(jj);
						G_index += this->_numStates;
						val_vec.segment(G_index, this->_numControls) = Jac_uk.row(jj);
						G_index += this->_numControls;
						val_vec.segment(G_index, this->_numControls) = Jac_ukp1.row(jj);
						G_index += this->_numControls;

						constraint_row++;
					}
					inc_index++;
				}
			phase_first_node += this->number_nodes_phase[p];
			}
		}

	    if (constraint_row!= this->num_defects) {
			std::cout << "Error evaluating the Jacobian of constraints (DEFECTS)" << std::endl;
			exit(EXIT_FAILURE);
		}

	// parametric constraints
		// Time constraint
		val_vec.segment(G_index, this->n_inc) = Eigen::VectorXd::Ones(this->n_inc);
		G_index += this->n_inc;
		constraint_row++;

		// increments constraint
		nnz_per_row = 2;
		if (this->_evenTimeIncr) {

			for (int k = 0 ; k < this->n_inc -1 ; k++) {

				Eigen::VectorXd aux_value(nnz_per_row);
				aux_value << 1 , -1;
				val_vec.segment(G_index,nnz_per_row) = aux_value;

				G_index+=nnz_per_row;
				constraint_row++;
			}
		}

		if (constraint_row != this->num_parametric_constraints + this->num_defects) {
			std::cout << "Error evaluating the Jacobian of constraints (PARAMETRIC)" << std::endl;
			std::cout << "contratint_row : " << constraint_row << std::endl;
			std::cout << "num_parametric_constraints : " << this->num_parametric_constraints << std::endl;
			exit(EXIT_FAILURE);
		}

    // User Constraints
		for (int k=0; k< this->_numberOfNodes; k++) {

			val_vec.segment(G_index,this->num_nonzero_jacobian_user_per_node_in_phase[this->node_to_phase_map[k]]) =
					this->_constraints[this->node_to_phase_map[k]]->evalConstraintsFctDerivatives(this->_y_trajectory[k], this->_u_trajectory[k]);
			G_index += this->num_nonzero_jacobian_user_per_node_in_phase[this->node_to_phase_map[k]];
			constraint_row += this->num_user_constraints_per_node_in_phase[this->node_to_phase_map[k]];
		}

		if (this->_multiphaseproblem) {

			state_vector_t gJac;
			/* Guard Constraints */
			for(int i = 0; i < this->num_phases -1 ; i++) {

			    // nnz per guard
			    int nnz_per_guard = this->_numStates*this->size_guard[i];

					// state at the left
					state_vector_t   y_l = this->_y_trajectory[this->leftphase_nodes(i)];

					// guard->evalJacobian should return a single-row Jacobian
					val_vec.segment(G_index,nnz_per_guard) = this->_guards[i]->evalConstraintsFctDerivatives(y_l);
					G_index += nnz_per_guard;
					constraint_row += this->size_guard[i];

			}

			/* Interphase constraints */

			for(int i = 0 ; i < this->num_phases - 1 ; i++) {
				int nnz_interphase_constraint = 2 * (this->_numStates + this->_numControls) * this->size_interphase_constraint[i];

				//ToDo: Set this outside this method!
				state_vector_t   y_l = this->_y_trajectory[this->leftphase_nodes(i)];
				control_vector_t u_l = this->_u_trajectory[this->leftphase_nodes(i)];
				state_vector_t   y_r = this->_y_trajectory[this->rightphase_nodes(i)];
				control_vector_t u_r = this->_u_trajectory[this->rightphase_nodes(i)];

				// _interphase_constraint->getJacobian() should return Jacobian matrix row-wise
				val_vec.segment(G_index,nnz_interphase_constraint) = this->_interphase_constraint[i]->evalConstraintsFctDerivatives(y_l,y_r,u_l,u_r);
				G_index += nnz_interphase_constraint;
				constraint_row += this->size_interphase_constraint[i];
			}

		}

    if (constraint_row!=this->myNLP.num_F) {
    	std::cout << "Error evaluating the Jacobian of constraints (USER)" << std::endl;
    	std::cout << "constraint_row : " << constraint_row << std::endl;
    	std::cout << "num_F : " << this->myNLP.num_F << std::endl;
    	std::cout << "lenG : " << this->myNLP.lenG << std::endl;
		exit(EXIT_FAILURE);
    }
}

} // namespace DirectTrajectoryOptimization
